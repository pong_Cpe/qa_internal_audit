<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GroupsController;
use App\Http\Controllers\DepsController;
use App\Http\Controllers\BrcSetsController;
use App\Http\Controllers\BrcGroupsController;
use App\Http\Controllers\PlanSetsController;
use App\Http\Controllers\PlanSetDetailsController;
use App\Http\Controllers\PlanSetDetailUsesController;
use App\Http\Controllers\BrcTopicsController;
use App\Http\Controllers\AuditsController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CarListsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('admin/groups/updateGroup/{id}', [GroupsController::class, 'updateGroup']);
Route::post('admin/groups/updateGroupAction/{id}', [GroupsController::class, 'updateGroupAction']);
Route::get('brctopics/addtopic/{id}', [BrcTopicsController::class, 'addtopic']);
Route::post('brctopics/addtopicAction/{id}', [BrcTopicsController::class, 'addtopicAction']);
Route::get('brctopics/edittopic/{brcsetid}/{detailid}', [BrcTopicsController::class, 'edittopic']);
Route::post('brctopics/edittopicAction/{brcsetid}/{detailid}', [BrcTopicsController::class, 'edittopicAction']);
Route::get('brctopics/deleteall/{brctopicid}', [BrcTopicsController::class, 'deleteall']);
Route::get('brcsets/showwithgroup/{brcsetid}/{brcgroupid}', [BrcSetsController::class,'showwithgroup']);
Route::get('plansetdetails/createwithplan/{id}', [PlanSetDetailsController::class, 'createwithplan']);
Route::post('plansetdetails/createwithplanAction/{id}', [PlanSetDetailsController::class, 'createwithplanAction']);
Route::get('plansetdetails/deletedetail/{id}', [PlanSetDetailsController::class, 'deletedetail']);
Route::get('plansetdetailusers/adduser/{id}', [PlanSetDetailUsesController::class, 'adduser']);
Route::post('plansetdetailusers/adduserAction/{id}', [PlanSetDetailUsesController::class, 'adduserAction']);
Route::get('plansetdetailusers/deleteuser/{id}', [PlanSetDetailUsesController::class, 'deleteuser']);
Route::get('plansets/setactive/{id}', [PlanSetsController::class, 'setactive']);


Route::get('audits/generateAudit/{id}', [AuditsController::class, 'generateAudit']);
Route::get('audits/checklists/{id}', [AuditsController::class, 'checklists']);
Route::post('audits/checklistsAction/{id}', [AuditsController::class, 'checklistsAction']);
Route::get('audits/list', [AuditsController::class, 'list']);
Route::get('audits/listbygroup/{plan_detail_set_id}', [AuditsController::class, 'listbygroup']);
Route::get('audits/checklistsbygroup/{id}/{brc_Group_id}', [AuditsController::class, 'checklistsbygroup']);
Route::post('audits/checklistsbygroupAction/{id}/{brc_Group_id}', [AuditsController::class, 'checklistsbygroupAction']);
Route::get('audits/reportaudit/{id}', [AuditsController::class, 'reportaudit']);
Route::get('audits/reportauditXLXS/{id}', [AuditsController::class, 'reportauditXLXS']);
Route::get('audits/checklistshows/{id}', [AuditsController::class, 'checklistshows']);
Route::get('audits/sendforclosecar/{id}', [AuditsController::class, 'sendforclosecar']);
Route::post('audits/sendforclosecarAction/{id}', [AuditsController::class, 'sendforclosecarAction']);
Route::get('audits/notelistsbygroup/{id}/{brc_Group_id}', [AuditsController::class, 'notelistsbygroup']);
Route::post('audits/notelistsbygroupAction/{id}/{brc_Group_id}', [AuditsController::class, 'notelistsbygroupAction']);
Route::get('audits/checkcarlistshows/{id}', [AuditsController::class, 'checkcarlistshows']);

Route::get('cars/list', [CarListsController::class, 'list']);
Route::get('cars/detail/{id}', [CarListsController::class, 'detail']);
Route::get('cars/reply/{id}', [CarListsController::class, 'reply']);
Route::post('cars/replyAction/{id}', [CarListsController::class, 'replyAction']);
Route::get('cars/replyGroup/{id}/{group}', [CarListsController::class, 'replyGroup']);
Route::post('cars/replyGroupAction/{id}/{group}', [CarListsController::class, 'replyGroupAction']);
Route::get('cars/checklistshows/{id}', [CarListsController::class, 'checklistshows'])->name('carchecklistshows');
Route::get('cars/sendforclosecar/{id}', [CarListsController::class, 'sendforclosecar']);
Route::post('cars/sendforclosecarAction/{id}', [CarListsController::class, 'sendforclosecarAction']);
Route::get('cars/mycarslist/{id}/{group}', [CarListsController::class, 'mycarslist']);
Route::get('cars/mycarslistforclose/{id}/{group}', [CarListsController::class, 'mycarslistforclose']);


Route::get('cars/checklistshowsforclose/{id}', [CarListsController::class, 'checklistshowsforclose'])->name('checklistshowsforclose');
Route::get('cars/changestatus/{id}/{status}', [CarListsController::class, 'changestatus'])->name('changestatus');
Route::post('cars/closecarAction/{carsetid}', [CarListsController::class, 'closecarAction']);
Route::get('cars/closethiscarset/{id}', [CarListsController::class, 'closethiscarset'])->name('closethiscarset');




Route::resource('admin/groups', GroupsController::class);
Route::resource('admin/deps', DepsController::class);
Route::resource('brcsets', BrcSetsController::class);
Route::resource('brctopics', BrcTopicsController::class);
Route::resource('brcgroups', BrcGroupsController::class);
Route::resource('plansets', PlanSetsController::class);
Route::resource('plansetdetails', PlanSetDetailsController::class);
Route::resource('plansetdetailusers', PlanSetDetailUsesController::class);
Route::get('/', [HomeController::class, 'index']);
Route::get('/test',function(){
    return view('/layouts/cleo');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
