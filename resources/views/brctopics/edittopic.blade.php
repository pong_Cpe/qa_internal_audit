@extends('layouts.theme')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Edit brctopic #{{ $brctopic->id }}</div>
                <div class="card-body">
                    <a href="{{ url('/brcsets/' . $brcsetid) }}" title="Back"><button class="btn btn-warning btn-sm"><i
                                class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <br />
                    <br />

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    <form method="POST" action="{{ url('/brctopics/edittopicAction/' . $brcsetid . '/' . $brctopic->id) }}"
                        accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">

                        {{ csrf_field() }}

                        @include ('brctopics.formwithgroup', ['formMode' => 'edit'])

                    </form>

                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('js/brctopic.js')}}"></script>
@endsection
