<div class="form-group {{ $errors->has('brc_set_id') ? 'has-error' : ''}}">
    <label for="brc_set_id" class="control-label">{{ 'BRC Set' }}</label>
    @if (isset($brctopic->brc_set_id))
        {{ Form::select('brc_set_id',$brcsets,$brctopic->brc_set_id,['placeholder'=>'==เลือก==','class'=>"form-control"]) }}
    @else
        @if (isset($brcid))
            {{ Form::select('brc_set_id',$brcsets,$brcid,['placeholder'=>'==เลือก==','class'=>"form-control"]) }} 
        @else
            {{ Form::select('brc_set_id',$brcsets,null,['placeholder'=>'==เลือก==','class'=>"form-control"]) }} 
        @endif       
    @endif   
    {!! $errors->first('brc_group_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('brc_group_id') ? 'has-error' : ''}}">
    <label for="brc_group_id" class="control-label">{{ 'BRC Set' }}</label>
    @if (isset($brctopic->brc_group_id))
        {{ Form::select('brc_group_id',$brcgroups,$brctopic->brc_group_id,['placeholder'=>'==เลือก==','class'=>"form-control"]) }}
    @else
       {{ Form::select('brc_group_id',$brcgroups,null,['placeholder'=>'==เลือก==','class'=>"form-control"]) }} 
    @endif   
    {!! $errors->first('brc_group_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('seq') ? 'has-error' : ''}}">
    <label for="seq" class="control-label">{{ 'Seq' }}</label>
    <input class="form-control" name="seq" type="number" id="seq" value="{{ isset($brctopic->seq) ? $brctopic->seq : ''}}" >
    {!! $errors->first('seq', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ref') ? 'has-error' : ''}}">
    <label for="ref" class="control-label">{{ 'Ref' }}</label>
    <input class="form-control" name="ref" type="text" id="ref" value="{{ isset($brctopic->ref) ? $brctopic->ref : ''}}" >
    {!! $errors->first('ref', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('topic') ? 'has-error' : ''}}">
    <label for="topic" class="control-label">{{ 'Topic' }}</label>
    <textarea class="form-control" rows="5" name="topic" type="textarea" id="topic" >{{ isset($brctopic->topic) ? $brctopic->topic : ''}}</textarea>
    {!! $errors->first('topic', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
    <label for="type" class="control-label">{{ 'Type' }}</label>
    <input class="form-control" name="type" type="text" id="type" value="{{ isset($brctopic->type) ? $brctopic->type : ''}}" >
    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'Desc' }}</label>
    <textarea class="form-control" rows="5" name="desc" type="textarea" id="desc" >{{ isset($brctopic->desc) ? $brctopic->desc : ''}}</textarea>
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="control-label">{{ 'Status' }}</label>
    <input class="form-control" name="status" type="text" id="status" value="{{ isset($brctopic->status) ? $brctopic->status : 'Active'}}" >
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
