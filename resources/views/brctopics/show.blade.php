@extends('layouts.theme')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">brctopic {{ $brctopic->id }}</div>
                <div class="card-body">

                    <a href="{{ url('/brctopics') }}" title="Back"><button class="btn btn-warning btn-sm"><i
                                class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <a href="{{ url('/brctopics/' . $brctopic->id . '/edit') }}" title="Edit brctopic"><button
                            class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            Edit</button></a>

                    <form method="POST" action="{{ url('brctopics' . '/' . $brctopic->id) }}" accept-charset="UTF-8"
                        style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger btn-sm" title="Delete brctopic"
                            onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                                aria-hidden="true"></i> Delete</button>
                    </form>
                    <br />
                    <br />

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $brctopic->id }}</td>
                                </tr>
                                <tr>
                                    <th> Seq </th>
                                    <td> {{ $brctopic->seq }} </td>
                                </tr>
                                <tr>
                                    <th> Ref </th>
                                    <td> {{ $brctopic->ref }} </td>
                                </tr>
                                <tr>
                                    <th> Topic </th>
                                    <td> {{ $brctopic->topic }} </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
