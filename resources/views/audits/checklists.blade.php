@extends('layouts.theme')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>Internal Audit Check List [ALL]</h2>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ url('/audits/checklistsAction/' . $plansetdetail->id) }}"
                        accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-7">
                                <h3><strong>แผนก / ฝ่ายที่ถูก audit : {{ $plansetdetail->dep->name }}</strong></h3>
                            </div>
                            <div class="col-md-5">
                                <h3><strong>วันที่ audit : {{ date('Y-m-d') }}</strong></h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 nomobile"><strong>ข้อกำหนด</strong></div>
                            <div class="col-md-3 nomobile"><strong>Check list</strong></div>
                            <div class="col-md-4 nomobile"><strong>ผลการตรวจสอบ</strong></div>
                            @foreach ($auditChks as $item)
                                <div class="col-md-5 
                                @if ($item->brctopic->type == 'เป็น')
                                bg-fundamental
                                @endif 
                                ">
                                    @if ($item->brctopic->type == 'เป็น')
                                        <img src="{{ asset('icons/star.png') }}"
                                            width="20px" /><strong>{{ $item->brctopic->ref ?? '' }}</strong>
                                    @else
                                        {{ $item->brctopic->ref ?? '' }}
                                    @endif
                                </div>
                                <div class="col-md-3 
                                @if ($item->brctopic->type == 'เป็น')
                                bg-fundamental
                                @endif 
                                "><strong>{{ $loop->iteration }}.
                                        {{ $item->brctopic->topic ?? '' }}</strong><br/><br/>

                                        <strong>ประเภท CAR</strong><br/>
                                    @if (!empty($item->car_type))
                                        {{ Form::select('auditchk_car_type_' . $item->id, $checklistcar, $item->car_type, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::select('auditchk_car_type_' . $item->id, $checklistcar, null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @endif
                                    </div>

                                <div class="col-md-4 
                                @if ($item->brctopic->type == 'เป็น')
                                bg-fundamental
                                @endif 
                                ">
                                    @if (!empty($item->ans))
                                        {{ Form::select('auditchk_ans_' . $item->id, $checklistans, $item->ans, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::select('auditchk_ans_' . $item->id, $checklistans, null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @endif
                                    @if (!empty($item->note))
                                        {{ Form::textarea('auditchk_note_' . $item->id, $item->note, ['placeholder' => 'รายละเอียด', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::textarea('auditchk_note_' . $item->id, null, ['placeholder' => 'รายละเอียด', 'class' => 'form-control']) }}
                                    @endif
                                    {!! Form::file('auditchk_pic_' . $item->id, $attributes = ['accept' => 'image/jpeg , image/jpg, image/gif, image/png']) !!}
                                    @if (!empty($item->pic_path))
                                        <a href="{{ url($item->pic_path) }}" target="_blank"><img height="20px"
                                                src="{{ url($item->pic_path) }}"></a>
                                        {!! Form::checkbox('remove_path_' . $item->id, 'remove', false) !!} remove
                                    @endif
                                </div>

                            @endforeach
                            <div class="col-md-12"><input class="btn btn-success" type="submit" value="บันทึก"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
