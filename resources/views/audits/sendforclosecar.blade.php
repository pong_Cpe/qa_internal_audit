@extends('layouts.theme')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>ส่งผลการ Audit ไปยังแผนก {{ $plansetdetail->dep->name }} </h2>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ url('/audits/sendforclosecarAction/' . $plansetdetail->id) }}"
                        accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-7">
                                <h3><strong>แผนก / ฝ่ายที่ถูก audit : {{ $plansetdetail->dep->name }}</strong></h3>
                            </div>
                            <div class="col-md-5">
                                <h3><strong>วันที่ audit : {{ date('Y-m-d') }}</strong></h3>
                            </div>
                        </div>
                    <div class="row">
                        <div class="col-md-6 form-group {{ $errors->has('deathline') ? 'has-error' : ''}}">
                            <label for="deathline" class="control-label">{{ 'วันเวลาต้องตอบกลับ' }}</label>
                            <input class="form-control" name="deathline" type="datetime-local" id="deathline" value="" >
                            {!! $errors->first('deathline', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="col-md-6 form-group {{ $errors->has('note') ? 'has-error' : ''}}">
                            <label for="note" class="control-label">{{ 'Note' }}</label>
                            <textarea class="form-control" rows="5" name="note" type="textarea" id="note" ></textarea>
                            {!! $errors->first('note', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                        <div class="row">
                            
                            <div class="col-md-12"><input class="btn btn-success" type="submit" value="ส่ง CAR"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
