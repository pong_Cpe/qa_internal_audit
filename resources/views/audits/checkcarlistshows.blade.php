@extends('layouts.theme')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>Review - Internal Audit Check List [CAR]</h2>
                        <a href="{{ url('audits/listbygroup/'.$plansetdetail->id) }}" class="btn btn-primary" > BACK </a>    
                           
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-7">
                            <h3><strong>แผนก / ฝ่ายที่ถูก audit : {{ $plansetdetail->dep->name }}</strong></h3>
                        </div>
                        <div class="col-md-5">
                            <h3><strong>วันที่ audit : {{ date('Y-m-d') }}</strong></h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 nomobile"><strong>ข้อกำหนด</strong></div>
                        <div class="col-md-3 nomobile"><strong>Check list</strong></div>
                        <div class="col-md-4 nomobile"><strong>[สถานะ] ผลการตรวจสอบ</strong></div>
                        @php
                            $alldata = array();
                            $alldata['all'] = 0;
                            $alldata['done'] = 0;
                        @endphp
                        @foreach ($auditChks as $item)
                            @php
                                $alldata['all']++;
                                if($item->status == 'Process'){
                                    $alldata['done']++;
                                }                            
                            @endphp
                            <div class="col-md-5 
                                    @if ($item->brctopic->type == 'เป็น') bg-fundamental @endif
                                ">
                                @if ($item->brctopic->type == 'เป็น')
                                    <img src="{{ asset('icons/star.png') }}"
                                        width="20px" /><strong>{{ $item->brctopic->ref ?? '' }}</strong>
                                @else
                                    {{ $item->brctopic->ref ?? '' }}
                                @endif
                            </div>
                            <div class="col-md-3 
                                    @if ($item->brctopic->type == 'เป็น') bg-fundamental @endif
                                "><strong>{{ $loop->iteration }}.
                                    {{ $item->brctopic->topic ?? '' }}</strong>

                                     @if (!empty($item->review))
                                            <br/><br/>
                                            <strong>Note</strong><br/>
                                            {{ $item->review }}
                                        @endif
                                        @if (!empty($item->reviewfile_path))
                                            <br/>File : <a href="{{ url($item->reviewfile_path) }}" target="_blank">{{$item->reviewfile}}</a>
                                        @endif
<br /><br />
                                <strong>ประเภท CAR</strong><br />
                                @if (!empty($item->car_type))
                                    {{ $checklistcar[$item->car_type] }}
                                @else
                                    -
                                @endif

                                
                            </div>

                            <div class="col-md-4 
                                    @if ($item->brctopic->type == 'เป็น') bg-fundamental @endif
                                ">



                                @if ($item->status == 'done')
                                    <span class="bg-success color-white">[{{ $item->status }}]</span>
                                @else
                                    <span class="bg-danger color-white">[{{ $item->status }}]</span>
                                @endif


                                <br />
                                @if (!empty($item->ans))
                                    {{ $checklistans[$item->ans] }}
                                @else
                                    -
                                @endif
                                <br /><strong>รายละเอียด:</strong><br />
                                @if (!empty($item->note))
                                    {{ $item->note }}
                                @else
                                    -
                                @endif
                                @if (!empty($item->pic_path))
                                    <br /><a href="{{ url($item->pic_path) }}" target="_blank"><img height="150px"
                                            src="{{ url($item->pic_path) }}"></a>
                                @endif
                            </div>

                        @endforeach
                        <div class="col-md-12"><br/>
                            @if ($alldata['done'] >= $alldata['all'])
                                @if ($carsetno > 0)
                                รอตอบ Car
                                @else 
                                <a href="{{ url('audits/sendforclosecar/'.$plansetdetail->id) }}" class="btn btn-primary" > ส่งผลการตรวจสอบให้แผนก </a>
                                @endif
                                    
                            @else 
                                ยังทำข้อมูลไม่ครบ
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
