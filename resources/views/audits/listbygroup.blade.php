@extends('layouts.theme')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3>Audit List</h3>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-3">
                                <strong>Department</strong> : {{ $plansetdetail->dep->name }}
                            </div>
                            <div class="col-3">
                                <strong>Lead Auditor</strong> : {{ $plansetdetail->leadauditor->name ?? '-' }}
                            </div>
                            <div class="col-3">
                                <strong>Auditor</strong> :
                                @foreach ($plansetdetail->auditors()->get() as $auditorObj)
                                    {{ $auditorObj->auditor->name }} ,
                                @endforeach
                            </div>
                            <div class="col-3">
                                <strong>Auditee</strong> : {{ $plansetdetail->auditee->name ?? '-' }}
                            </div>
                            <div class="col-3">
                                <strong>Plan</strong> : {{ date('Y-m-d H:i',strtotime($plansetdetail->plan_start))}}<br />
                                {{ date('Y-m-d H:i',strtotime($plansetdetail->plan_end)) }}
                            </div>
                            <div class="col-3">
                                <strong>CAR</strong> :
                                {{ date('Y-m-d H:i',strtotime($plansetdetail->act_start)) }}<br />{{ date('Y-m-d H:i',strtotime($plansetdetail->act_end)) }}
                            </div>

                            <div class="col-3">
                                <strong>Status</strong> :
                                {{ $plansetdetail->status}}
                                </div>
                            <div class="col-3">
                                <strong>Audits No / Done </strong> : {{ $plansetdetail->audits()->count() }} /
                                @if (isset($auditByall['done']))
                                    {{ sizeof($auditByall['done']) }}
                                @else
                                    0
                                @endif
                                <br />
                                <a href="{{ url('/audits/checklistshows/' . $plansetdetail->id . '?prevurl=' . URL::current()) }}"
                                    title="Edit plansetdetail"><button class="btn btn-warning btn-sm">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Review [All]</button></a>
                                @if ($plansetdetail->status != 'Close')
                                    @if (isset($auditByall['done']) && $plansetdetail->audits()->count() == sizeof($auditByall['done']))
                                    
                                            <a href="{{ url('/audits/checkcarlistshows/' . $plansetdetail->id . '?prevurl=' . URL::current()) }}"
                                        title="Edit plansetdetail"><button class="btn btn-secondary btn-sm">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Review [Car]</button></a>
                                    
                                    @endif    
                                @endif
                                
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>BRC Group</th>
                                        <th>Audits No / Done</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($auditByGroup as $brcgroup => $item)
                                        <tr>
                                            <td>{{ $brcgroup }}</td>
                                            <td>{{ sizeof($item['all']) }}
                                                /
                                                @if (isset($item['done']))
                                                    {{ sizeof($item['done']) }}
                                                @else
                                                    0
                                                @endif
                                            </td>
                                            <td>
                                                @if ($plansetdetail->status != 'CAR')
                                                    @if ($plansetdetail->status == 'Close')
                                                        รอ QMR Review
                                                    @else
                                                       <a href="{{ url('/audits/notelistsbygroup/' . $item['plan_detail_set_id'] . '/' . $item['brc_group_id'] . '?prevurl=' . URL::current()) }}"
                                                        title="Edit plansetdetail"><button class="btn btn-primary btn-sm"><i
                                                                class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                            Preview</button></a>
                                                    <a href="{{ url('/audits/checklistsbygroup/' . $item['plan_detail_set_id'] . '/' . $item['brc_group_id'] . '?prevurl=' . URL::current()) }}"
                                                        title="Edit plansetdetail"><button class="btn btn-success btn-sm"><i
                                                                class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                            Audit</button></a> 
                                                    @endif
                                                    
                                                @else

                                                    รอตอบ Car

                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
