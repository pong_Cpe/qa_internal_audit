@extends('layouts.theme')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Audit List</div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Department</th>
                                    <th>Lead Auditor</th>
                                    <th>Auditor</th>
                                    <th>Auditee</th>
                                    <th>Plan / Act</th>
                                    <th>Audits No. / Done</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($plansetdetails as $item)
                                    <tr>
                                        <td>{{ $item->dep->name }}</td>
                                        <td>{{ $item->leadauditor->name ?? '-' }}</td>
                                        <td>
                                            @foreach ($item->auditors()->get() as $auditorObj)
                                                {{ $auditorObj->auditor->name }}
                                                <a href="{{ url('/plansetdetailusers/deleteuser/' . $auditorObj->id) }}"
                                                    title="Delete Auditor"
                                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><button
                                                        class="btn btn-danger btn-sm"><i class="fa fa-pencil-square-o"
                                                            aria-hidden="true"></i> Delete</button></a><br />

                                            @endforeach


                                            <a href="{{ url('/plansetdetailusers/adduser/' . $item->id) }}"
                                                title="Add Auditor"><button class="btn btn-primary btn-sm"><i
                                                        class="fa fa-pencil-square-o" aria-hidden="true"></i> Add
                                                    Auditor</button></a>
                                        </td>
                                        <td>{{ $item->auditee->name ?? '-' }}</td>
                                        <td>{{ $item->plan_start }} - {{ $item->plan_end }} <br />/
                                            {{ $item->act_start }} - {{ $item->act_end }}</td>
                                        <td>
                                            <a href="{{ url('/audits/checklists/' . $item->id) }}"
                                                title="Edit plansetdetail">
                                                {{ $item->audits()->count() }}</a>
                                        </td>
                                        <td>
                                            @if ($item->audits()->count() == 0)
                                                
                                            @else
                                                <a href="{{ url('/audits/checklists/' . $item->id . '?prevurl=' . URL::current()) }}"
                                                    title="Edit plansetdetail"><button class="btn btn-success btn-sm"><i
                                                            class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                        Audit</button></a>
                                            @endif


                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $plansetdetails->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
