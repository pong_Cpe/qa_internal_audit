@extends('layouts.theme')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>Internal Audit Check List [ALL]</h2>
                    <a href="{{ url('audits/reportauditXLXS/'.$plansetdetail->id) }}" class="btn btn-success" >Download
                    </a>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-7">
                            <h3><strong>แผนก / ฝ่ายที่ถูก audit : {{ $plansetdetail->dep->name }}</strong></h3>
                        </div>
                        <div class="col-md-5">
                            <h3><strong>วันที่ audit : {{ date('Y-m-d') }}</strong></h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 nomobile"><strong>Check list</strong></div>
                        <div class="col-md-5 nomobile"><strong>ข้อกำหนด</strong></div>
                        <div class="col-md-4 nomobile"><strong>ผลการตรวจสอบ</strong></div>
                        @foreach ($auditChks as $item)
                            <div class="col-md-3"><strong>{{ $loop->iteration }}.
                                    {{ $item->brctopic->topic ?? '' }}</strong></div>
                            <div class="col-md-5">{{ $item->brctopic->ref ?? '' }}</div>
                            <div class="col-md-4">
                                @if (!empty($item->ans))
                                    {{ $checklistans[$item->ans] }}
                                @else
                                    -
                                @endif
                                <br />
                                @if (!empty($item->note))
                                    {{ $item->note }}
                                @else
                                    -
                                @endif
                                <br />
                                @if (!empty($item->pic_path))
                                    <a href="{{ url($item->pic_path) }}" target="_blank">
                                        <img height="80px" src="{{ url($item->pic_path) }}">
                                    </a>
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
