@extends('layouts.theme')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>CAR List [ALL]</h2>

                </div>
                <form method="POST" action="{{ url('/cars/closecarAction/' . $carset->id) }}" accept-charset="UTF-8"
                    class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-7">
                                <h3><strong>แผนก / ฝ่ายที่ถูก audit : {{ $plansetdetail->dep->name }}</strong></h3>
                            </div>
                            <div class="col-md-5">
                                <h3><strong>วันที่ audit : {{ date('Y-m-d') }}</strong></h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 nomobile"><strong>ข้อกำหนด</strong></div>
                            <div class="col-md-3 nomobile"><strong>Check list</strong></div>
                            <div class="col-md-4 nomobile"><strong>[สถานะ] ผลการตรวจสอบ</strong></div>


                            @php
                                $alldata = [];
                                $alldata['all'] = 0;
                                $alldata['done'] = 0;
                            @endphp
                            @foreach ($auditChks as $item)
                                @php
                                    $alldata['all']++;
                                    if ($item->status == 'done') {
                                        $alldata['done']++;
                                    }
                                @endphp
                                <div class="col-md-5 
                                        @if ($item->brctopic->type == 'เป็น') bg-fundamental @endif
                                    ">
                                    @if ($item->brctopic->type == 'เป็น')
                                        <img src="{{ asset('icons/star.png') }}"
                                            width="20px" /><strong>{{ $item->brctopic->ref ?? '' }}</strong>
                                    @else
                                        {{ $item->brctopic->ref ?? '' }}
                                    @endif
                                </div>
                                <div class="col-md-3 
                                        @if ($item->brctopic->type == 'เป็น') bg-fundamental @endif
                                    "><strong>{{ $loop->iteration }}.
                                        {{ $item->brctopic->topic ?? '' }}</strong>
                                        
                                        
                                         @if (!empty($item->review))
                                        <br/><br/>
                                        <strong>Note</strong><br/>
                                        {{ $item->review }}
                                    @endif
                                    @if (!empty($item->reviewfile_path))
                                        <br/>File : <a href="{{ url($item->reviewfile_path) }}" target="_blank">{{$item->reviewfile}}</a>
                                    @endif
                                    <br /><br />
                                    
                                    <strong>ประเภท CAR</strong><br />
                                    @if (!empty($item->car_type))
                                        {{ $checklistcar[$item->car_type] }}
                                    @else
                                        -
                                    @endif
                                </div>

                                <div class="col-md-4 
                                        @if ($item->brctopic->type == 'เป็น') bg-fundamental @endif
                                    ">
                                    @if ($item->status == 'done')
                                        <span class="bg-success color-white">[{{ $item->status }}]</span>
                                    @else
                                        <span class="bg-danger color-white">[{{ $item->status }}]</span>
                                    @endif


                                    <br />
                                    @if (!empty($item->ans))
                                        {{ $checklistans[$item->ans] }}
                                    @else
                                        -
                                    @endif
                                    <br /><strong>รายละเอียด:</strong><br />
                                    @if (!empty($item->note))
                                        {{ $item->note }}
                                    @else
                                        -
                                    @endif
                                    @if (!empty($item->pic_path))
                                        <br /><a href="{{ url($item->pic_path) }}" target="_blank"><img height="150px"
                                                src="{{ url($item->pic_path) }}"></a>
                                    @endif
                                </div>

                                @if (isset($carlistdata[$item->id]))
                                <div class="col-md-12">
                                    <strong>วันที่</strong> {{ $carlistdata[$item->id]->car_date }}
                                </div>
                                <div class="col-md-4">
                                    <strong>Cause</strong><br/>{{ $carlistdata[$item->id]->correction }}
                                    @if (!empty($carlistdata[$item->id]->pic1_path))
                                        <br /><a href="{{ url($carlistdata[$item->id]->pic1_path) }}" target="_blank"><img width="150px"
                                                src="{{ url($carlistdata[$item->id]->pic1_path) }}"></a>
                                        @endif
                                </div>
                                <div class="col-md-4">
                                    <strong>Correction & Corrective Action</strong><br/>{{ $carlistdata[$item->id]->corrective_action }}
                                    @if (!empty($carlistdata[$item->id]->pic2_path))
                                        <br /><a href="{{ url($carlistdata[$item->id]->pic2_path) }}" target="_blank"><img width="150px"
                                                src="{{ url($carlistdata[$item->id]->pic2_path) }}"></a>
                                        @endif
                                </div>
                                <div class="col-md-4">
                                    <strong>Prevention</strong><br/>{{ $carlistdata[$item->id]->prevention }}
                                    @if (!empty($carlistdata[$item->id]->pic3_path))
                                        <br /><a href="{{ url($carlistdata[$item->id]->pic3_path) }}" target="_blank"><img width="150px"
                                                src="{{ url($carlistdata[$item->id]->pic3_path) }}"></a>
                                        @endif
                                </div>
                                <div class="col-md-12">
                                    <div class="form-check p-3">
                                        <input type="checkbox" class="form-check-input" id="close-{{$item->id}}"  name="close-{{$item->id}}" value="{{$item->id}}"
                                        @if (!empty($carlistdata[$item->id]->status) && $carlistdata[$item->id]->status == 'Close')
                                            checked 
                                        @endif
                                        >
                                        <label class="form-check-label" for="close-{{$item->id}}">ปิด Car</label>
                                        <input type="text" placeholder="รายละเอียดเพิ่มเติม " class="form-control"  name="feedbackcar-{{$item->id}}" id="feedbackcar-{{$item->id}}"                                        
                                        @if (!empty($carlistdata[$item->id]->feedbackcar))
                                            value="{{$carlistdata[$item->id]->feedbackcar}}" 
                                        @endif
                                        >
                                    </div>
                                </div>
                                @else
                                <div class="col-md-12">
                                    <strong>วันที่</strong> -
                                </div>

                                <div class="col-md-4">
                                    <strong>Cause</strong> -
                                </div>
                                <div class="col-md-4">
                                    <strong>Correction & Corrective Action</strong> -
                                </div>
                                <div class="col-md-4">
                                    <strong>Prevention</strong> - 
                                </div>    
                                @endif
                                
                                
                            @endforeach
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    @if ($auditChks->count() > 0)
                                        <button type="submit" class="btn btn-success">ส่งผลการตรวจสอบ</button>
                                    @else
                                        <a href="{{ url('cars/closethiscarset/'.$carset->id) }}" class="btn btn-success"> Close Audits</a>
                                    @endif
                                 
                            </div>


                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
