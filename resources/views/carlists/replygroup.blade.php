@extends('layouts.theme')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>CAR List [{{ $brcgroup->name }}]</h2>

                </div>
                <form method="POST" action="{{ url('/cars/replyGroupAction/' . $carset->id.'/'.$brcgroup->id) }}" accept-charset="UTF-8"
                    class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-7">
                                <h3><strong>แผนก / ฝ่ายที่ถูก audit : {{ $plansetdetail->dep->name }}</strong></h3>
                            </div>
                            <div class="col-md-5">
                                <h3><strong>วันที่ audit : {{ date('Y-m-d') }}</strong></h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 nomobile"><strong>ข้อกำหนด</strong></div>
                            <div class="col-md-3 nomobile"><strong>Check list</strong></div>
                            <div class="col-md-4 nomobile"><strong>[สถานะ] ผลการตรวจสอบ</strong></div>


                            @php
                                $alldata = [];
                                $alldata['all'] = 0;
                                $alldata['done'] = 0;
                            @endphp
                            @foreach ($auditChks as $item)
                                @php
                                    $alldata['all']++;
                                    if ($item->status == 'done') {
                                        $alldata['done']++;
                                    }
                                @endphp
                                <div class="col-md-5 
                                        @if ($item->brctopic->type == 'เป็น') bg-fundamental @endif
                                    ">
                                    @if ($item->brctopic->type == 'เป็น')
                                        <img src="{{ asset('icons/star.png') }}"
                                            width="20px" /><strong>{{ $item->brctopic->ref ?? '' }}</strong>
                                    @else
                                        {{ $item->brctopic->ref ?? '' }}
                                    @endif
                                </div>
                                <div class="col-md-3 
                                        @if ($item->brctopic->type == 'เป็น') bg-fundamental @endif
                                    "><strong>{{ $loop->iteration }}.
                                        {{ $item->brctopic->topic ?? '' }}</strong>
                                    @if (!empty($item->review))
                                        <br/><br/>
                                        <strong>Note</strong><br/>
                                        {{ $item->review }}
                                    @endif
                                    @if (!empty($item->reviewfile_path))
                                        <br/>File : <a href="{{ url($item->reviewfile_path) }}" target="_blank">{{$item->reviewfile}}</a>
                                    @endif
                                    <br /><br />
                                    <strong>ประเภท CAR</strong><br />
                                    @if (!empty($item->car_type))
                                        {{ $checklistcar[$item->car_type] }}
                                    @else
                                        -
                                    @endif
                                </div>
                                <div class="col-md-4 
                                        @if ($item->brctopic->type == 'เป็น') bg-fundamental @endif
                                    ">
                                    @if ($item->status == 'done')
                                        <span class="bg-success color-white">[{{ $item->status }}]</span>
                                    @else
                                        <span class="bg-danger color-white">[{{ $item->status }}]</span>
                                    @endif


                                    <br />
                                    @if (!empty($item->ans))
                                        {{ $checklistans[$item->ans] }}
                                    @else
                                        -
                                    @endif
                                    <br /><strong>รายละเอียด:</strong><br />
                                    @if (!empty($item->note))
                                        {{ $item->note }}
                                    @else
                                        -
                                    @endif
                                    @if (!empty($item->pic_path))
                                        <br /><a href="{{ url($item->pic_path) }}" target="_blank"><img height="150px"
                                                src="{{ url($item->pic_path) }}"></a>
                                    @endif
                                </div>
                                <div class="col-md-12">
                                    {{ Form::date('car_date_' . $item->id, null, ['placeholder' => 'date', 'class' => 'form-control']) }}
                                </div>
                                @if (isset($carlistdata[$item->id]))
                                    @if ($item->status == 'Close')
                                    <div class="col-md-4">
                                        <strong>Cause</strong>
                                        {{ Form::textarea('car_correction_' . $item->id, $carlistdata[$item->id]->correction, ['placeholder' => 'Correction', 'class' => 'form-control','readonly' => true]) }}
                                        @if (!empty($carlistdata[$item->id]->pic1_path))
                                        <br /><a href="{{ url($carlistdata[$item->id]->pic1_path) }}" target="_blank"><img width="150px"
                                                src="{{ url($carlistdata[$item->id]->pic1_path) }}"></a>
                                        @endif
                                    </div>
                                    <div class="col-md-4">
                                        <strong>Correction & Corrective Action</strong>                    
                                        {{ Form::textarea('car_corrective_action_' . $item->id, $carlistdata[$item->id]->corrective_action, ['placeholder' => 'Corrective Action', 'class' => 'form-control','readonly' => true]) }}                                                                     
                                        @if (!empty($carlistdata[$item->id]->pic2_path))
                                        <br /><a href="{{ url($carlistdata[$item->id]->pic2_path) }}" target="_blank"><img width="150px"
                                                src="{{ url($carlistdata[$item->id]->pic2_path) }}"></a>
                                        @endif
                                    </div>
                                    <div class="col-md-4">
                                        <strong>Prevention</strong>
                                        {{ Form::textarea('car_prevention_' . $item->id, $carlistdata[$item->id]->prevention, ['placeholder' => 'Prevention', 'class' => 'form-control','readonly' => true]) }}
                                        @if (!empty($carlistdata[$item->id]->pic3_path))
                                        <br /><a href="{{ url($carlistdata[$item->id]->pic3_path) }}" target="_blank"><img width="150px"
                                                src="{{ url($carlistdata[$item->id]->pic3_path) }}"></a>
                                        @endif
                                    </div>
                                    @else
                                    <div class="col-md-4">
                                        <strong>Cause</strong>
                                        
                                            {{ Form::textarea('car_correction_' . $item->id, $carlistdata[$item->id]->correction, ['placeholder' => 'Correction', 'class' => 'form-control']) }}
                                        
                                        {!! Form::file('car_pic1_' . $item->id, $attributes = ['accept' => 'image/jpeg , image/jpg, image/gif, image/png']) !!}
                                        @if (!empty($carlistdata[$item->id]->pic1_path))
                                        <br /><a href="{{ url($carlistdata[$item->id]->pic1_path) }}" target="_blank"><img width="150px"
                                                src="{{ url($carlistdata[$item->id]->pic1_path) }}"></a>
                                        @endif
                                    </div>
                                    <div class="col-md-4">
                                        <strong>Correction & Corrective Action</strong>                    
                                        {{ Form::textarea('car_corrective_action_' . $item->id, $carlistdata[$item->id]->corrective_action, ['placeholder' => 'Corrective Action', 'class' => 'form-control']) }}                                                                     
                                        {!! Form::file('car_pic2_' . $item->id, $attributes = ['accept' => 'image/jpeg , image/jpg, image/gif, image/png']) !!}
                                        @if (!empty($carlistdata[$item->id]->pic2_path))
                                        <br /><a href="{{ url($carlistdata[$item->id]->pic2_path) }}" target="_blank"><img width="150px"
                                                src="{{ url($carlistdata[$item->id]->pic2_path) }}"></a>
                                        @endif
                                    </div>
                                    <div class="col-md-4">
                                        <strong>Prevention</strong>
                                        {{ Form::textarea('car_prevention_' . $item->id, $carlistdata[$item->id]->prevention, ['placeholder' => 'Prevention', 'class' => 'form-control']) }}
                                        {!! Form::file('car_pic3_' . $item->id, $attributes = ['accept' => 'image/jpeg , image/jpg, image/gif, image/png']) !!}
                                        @if (!empty($carlistdata[$item->id]->pic3_path))
                                        <br /><a href="{{ url($carlistdata[$item->id]->pic3_path) }}" target="_blank"><img width="150px"
                                                src="{{ url($carlistdata[$item->id]->pic3_path) }}"></a>
                                        @endif
                                    </div>
                                    @endif
                                @else
                                    <div class="col-md-4">
                                        <strong>Cause</strong>
                                        {{ Form::textarea('car_correction_' . $item->id, null, ['placeholder' => 'Correction', 'class' => 'form-control']) }}
                                        {!! Form::file('car_pic1_' . $item->id, $attributes = ['accept' => 'image/jpeg , image/jpg, image/gif, image/png']) !!}
                                    </div>
                                    <div class="col-md-4">
                                        <strong>Correction & Corrective Action</strong>                    
                                        {{ Form::textarea('car_corrective_action_' . $item->id, null, ['placeholder' => 'Corrective Action', 'class' => 'form-control']) }}                                                                     
                                        {!! Form::file('car_pic2_' . $item->id, $attributes = ['accept' => 'image/jpeg , image/jpg, image/gif, image/png']) !!}
                                    </div>
                                    <div class="col-md-4">
                                        <strong>Prevention</strong>
                                        {{ Form::textarea('car_prevention_' . $item->id, null, ['placeholder' => 'Prevention', 'class' => 'form-control']) }}
                                        {!! Form::file('car_pic3_' . $item->id, $attributes = ['accept' => 'image/jpeg , image/jpg, image/gif, image/png']) !!}
                                    </div>
                                @endif   
                            @endforeach
                            <div class="col-md-12">
                                <div class="col-md-12"><input class="btn btn-success" type="submit" value="บันทึก"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
