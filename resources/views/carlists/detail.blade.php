@extends('layouts.theme')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3>CAR List</h3>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-3">
                                <strong>Department</strong> : {{ $plansetdetail->dep->name }}
                            </div>
                            <div class="col-3">
                                <strong>Lead Auditor</strong> : {{ $plansetdetail->leadauditor->name ?? '-' }}
                            </div>
                            <div class="col-3">
                                <strong>Auditor</strong> :
                                @foreach ($plansetdetail->auditors()->get() as $auditorObj)
                                    {{ $auditorObj->auditor->name }} ,
                                @endforeach
                            </div>
                            <div class="col-3">
                                <strong>Auditee</strong> : {{ $plansetdetail->auditee->name ?? '-' }}
                            </div>
                            <div class="col-3">
                                <strong>Plan</strong> : {{ $plansetdetail->plan_start }}<br/>
                                {{ $plansetdetail->plan_end }}
                            </div>
                            <div class="col-3">
                                <strong>Act</strong> : {{ $plansetdetail->act_start }}<br/>{{ $plansetdetail->act_end }}
                            </div>
                             <div class="col-3">
                                <strong>วันตอบ CAR</strong> : {{ $carset->deathline }}<br/><strong>Audits No / CAR </strong> : {{ $plansetdetail->audits()->count() }} / 
                                @if (isset($auditByall['done']))
                                    {{ sizeof($auditByall['done']) }}
                                @else
                                    0
                                @endif
                            </div>
                            <div class="col-3">
                                <strong>Status</strong> :  {{ $carset->status }}
                                @if ($carset->status == 'New' || $carset->status == 'Recheck' )
                                <br/>
                                    <a href="{{ url('/cars/checklistshows/' . $carset->id . '?prevurl=' . URL::current()) }}"
                                    title="Edit plansetdetail"><button class="btn btn-warning btn-sm">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Review</button></a>
                                @endif
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>BRC Group</th>
                                        <th>Audits No / CAR</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($auditByGroup as $brcgroup => $item)
                                        <tr>
                                            <td>{{ $brcgroup }}</td>
                                            <td>{{ sizeof($item['all']) }}
                                                /
                                                @if (isset($carByGroup[$brcgroup]['done']))
                                                    {{ sizeof($carByGroup[$brcgroup]['done']) }}
                                                @else
                                                    0
                                                @endif
                                            </td>
                                            <td>
                                                @if ($carset->status == 'New' || $carset->status == 'Recheck' )
                                                <a href="{{ url('/cars/replyGroup/' . $carset->id . '/' . $item['brc_group_id'] . '?prevurl=' . URL::current()) }}"
                                                    title="Edit plansetdetail"><button class="btn btn-success btn-sm"><i
                                                            class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                        ตอบ CAR</button></a>    
                                                @else 
                                                รอ Review ตอบ CAR
                                                @endif
                                                

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
