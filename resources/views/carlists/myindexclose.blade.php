@extends('layouts.theme')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">CAR Lists</div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Set</th>
                                    <th>แผนก</th>
                                    <th>รอบ</th>
                                    <th>วันที่ต้องปิด</th>
                                    <th>status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($carsets as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->plansetdetail->planset->name }}</td>
                                        <td>{{ $item->dep->name }}</td>
                                        <td>{{ $item->round }}</td>
                                        <td>{{ $item->deathline }}</td>
                                        <td>{{ $item->status }}</td>
                                        <td>
                                        <td>
                                             <a href="{{ url('/cars/checklistshowsforclose/' . $item->id) }}" title="View brctopic"><button
                                                    class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i>
                                                    Review Car</button></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $carsets->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
