@extends('layouts.theme')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>ตอบกลับ CAR ของแผนก {{ $carset->plansetdetail->dep->name }} กลับไป Auditor</h2>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ url('/cars/sendforclosecarAction/' . $carset->id) }}"
                        accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-7">
                                <h3><strong>แผนก / ฝ่ายที่ถูก audit : {{ $carset->plansetdetail->dep->name }}</strong></h3>
                            </div>
                            <div class="col-md-5">
                                <h3><strong>วันที่ audit : {{ date('Y-m-d') }}</strong></h3>
                            </div>
                        </div>
                    <div class="row">
                        <div class="col-md-6 form-group {{ $errors->has('returncar_date') ? 'has-error' : ''}}">
                            <label for="returncar_date" class="control-label">{{ 'วันเวลาต้องตอบกลับ' }}</label>
                            <input class="form-control" name="returncar_date" type="datetime-local" id="returncar_date" value="" >
                            {!! $errors->first('returncar_date', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="col-md-6 form-group {{ $errors->has('returncar_note') ? 'has-error' : ''}}">
                            <label for="returncar_note" class="control-label">{{ 'รายละเอียดเพิ่มเติม' }}</label>
                            <textarea class="form-control" rows="5" name="returncar_note" type="textarea" id="returncar_note" ></textarea>
                            {!! $errors->first('returncar_note', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                        <div class="row">
                            
                            <div class="col-md-12"><input class="btn btn-success" type="submit" value="ส่ง CAR"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
