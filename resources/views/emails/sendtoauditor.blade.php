<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LACO - QA Internal Audit</title>
    <style>
        table {
            border: solid 2px black;
        }
        thead tr th{
            border: solid 1px black;
            font-weight: bold;
            font-style: italic;
        }
        tbody tr td{
            border: solid 1px black;
        }
    </style>
</head>
<body>
    <p><strong>TO..{{$mailObj['carset']->plansetdetail->dep->name}}</strong></p>
    <p><strong>ส่ง CAR มาให้แผนก {{$mailObj['carset']->plansetdetail->dep->name}}</strong></p>
    <p>Car รอบที่ {{$mailObj['carset']->round}}</p>
    <p>วันกำหนดส่งคือ {{$mailObj['carset']->deathline}} | ตอบกลับ {{$mailObj['carset']->returncar_date}}</p>
    <p><a href="{{url('/cars/detail/'.$mailObj['carset']->id)}}">Link เพื่อ Car set</a></p>
    <p><strong>BY LACO - QA Internal Audit</strong></p>
</body>
</html>