@extends('layouts.theme')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">plansetdetail {{ $plansetdetail->id }}</div>
                <div class="card-body">

                    <a href="{{ url('/plansetdetails') }}" title="Back"><button class="btn btn-warning btn-sm"><i
                                class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <a href="{{ url('/plansetdetails/' . $plansetdetail->id . '/edit') }}"
                        title="Edit plansetdetail"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                aria-hidden="true"></i> Edit</button></a>

                    <form method="POST" action="{{ url('plansetdetails' . '/' . $plansetdetail->id) }}"
                        accept-charset="UTF-8" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger btn-sm" title="Delete plansetdetail"
                            onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                                aria-hidden="true"></i> Delete</button>
                    </form>
                    <br />
                    <br />

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $plansetdetail->id }}</td>
                                </tr>
                                <tr>
                                    <th> Plan Start </th>
                                    <td> {{ $plansetdetail->plan_start }} </td>
                                </tr>
                                <tr>
                                    <th> Plan End </th>
                                    <td> {{ $plansetdetail->plan_end }} </td>
                                </tr>
                                <tr>
                                    <th> Act Start </th>
                                    <td> {{ $plansetdetail->act_start }} </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
