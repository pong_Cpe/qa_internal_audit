<div class="row">
<div class="form-group col-4 {{ $errors->has('plan_set_id') ? 'has-error' : ''}}">
    <label for="plan_set_id" class="control-label">{{ 'Plan Set' }}</label>
    @if (isset($plansetdetail->plan_set_id))
        {{ Form::select('plan_set_id',$plansets,$plansetdetail->plan_set_id,['placeholder'=>'==เลือก==','class'=>"selectpicker form-control", 'data-live-search'=>"true"]) }}
    @else
        @if (isset($planid))
            {{ Form::select('plan_set_id',$plansets,$planid,['placeholder'=>'==เลือก==','class'=>"selectpicker form-control", 'data-live-search'=>"true"]) }} 
        @else
            {{ Form::select('plan_set_id',$plansets,null,['placeholder'=>'==เลือก==','class'=>"selectpicker form-control", 'data-live-search'=>"true"]) }}      
        @endif
       
    @endif   
    {!! $errors->first('qmr_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group col-4 {{ $errors->has('dep_id') ? 'has-error' : ''}}">
    <label for="dep_id" class="control-label">{{ 'Department' }}</label>
    @if (isset($plansetdetail->dep_id))
        {{ Form::select('dep_id',$deps,$plansetdetail->dep_id,['placeholder'=>'==เลือก==','class'=>"selectpicker form-control", 'data-live-search'=>"true",'required'=>true]) }}
    @else
       {{ Form::select('dep_id',$deps,null,['placeholder'=>'==เลือก==','class'=>"selectpicker form-control", 'data-live-search'=>"true",'required'=>true]) }} 
    @endif   
    {!! $errors->first('dep_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group  col-4  {{ $errors->has('brc_set_id') ? 'has-error' : ''}}">
    <label for="brc_set_id" class="control-label">{{ 'BRC Set' }}</label>
    @if (isset($plansetdetail->brc_set_id))
        {{ Form::select('brc_set_id',$brcsets,$plansetdetail->brc_set_id,['placeholder'=>'==เลือก==','class'=>"selectpicker form-control", 'data-live-search'=>"true",'required'=>true]) }}
    @else
        @if (isset($plansetObj->brc_set_id))
            {{ Form::select('brc_set_id',$brcsets,$plansetObj->brc_set_id,['placeholder'=>'==เลือก==','class'=>"selectpicker form-control", 'data-live-search'=>"true",'required'=>true]) }} 
        @else
            {{ Form::select('brc_set_id',$brcsets,null,['placeholder'=>'==เลือก==','class'=>"form-control"]) }} 
        @endif

       
    @endif   
    {!! $errors->first('brc_set_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group  col-3  {{ $errors->has('plan_start') ? 'has-error' : ''}}">
    <label for="plan_start" class="control-label">{{ 'Plan Start' }}</label>
    <input class="form-control datepickst" required name="plan_start" type="datetime-local" id="plan_start" value="@php
            if(isset($plansetdetail->plan_start)){
                echo date('Y-m-d\TH:i',strtotime($plansetdetail->plan_start));
            }else{
                echo \Carbon\Carbon::now()->format('Y-m-d\TH:i');
            }
        @endphp" >
    {!! $errors->first('plan_start', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group  col-3  {{ $errors->has('plan_end') ? 'has-error' : ''}}">
    <label for="plan_end" class="control-label">{{ 'Plan End' }}</label>
    <input class="form-control datepickst" required name="plan_end" type="datetime-local" id="plan_end" value="@php
            if(isset($plansetdetail->plan_end)){
                echo date('Y-m-d\TH:i',strtotime($plansetdetail->plan_end));
            }else{
                echo \Carbon\Carbon::now()->format('Y-m-d\TH:i');
            }
        @endphp" >
    {!! $errors->first('plan_end', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group  col-3  {{ $errors->has('act_start') ? 'has-error' : ''}}">
    <label for="act_start" class="control-label">{{ 'Car Start' }}</label>
    <input class="form-control datepickst" required name="act_start" type="datetime-local" id="act_start" value="@php
            if(isset($plansetdetail->act_start)){
                echo date('Y-m-d\TH:i',strtotime($plansetdetail->act_start));
            }else{
                echo \Carbon\Carbon::now()->format('Y-m-d\TH:i');
            }
        @endphp" >
    {!! $errors->first('act_start', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group  col-3  {{ $errors->has('act_end') ? 'has-error' : ''}}">
    <label for="act_end" class="control-label">{{ 'Car End' }}</label>
    <input class="form-control datepickst" required name="act_end" type="datetime-local" id="act_end" value="@php
            if(isset($plansetdetail->act_end)){
                echo date('Y-m-d\TH:i',strtotime($plansetdetail->act_end));
            }else{
                echo \Carbon\Carbon::now()->format('Y-m-d\TH:i');
            }
        @endphp" >
    {!! $errors->first('act_end', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group  col-6  {{ $errors->has('lead_auditor_id') ? 'has-error' : ''}}">
    <label for="lead_auditor_id" class="control-label">{{ 'Lead Auditor' }}</label>
    @if (isset($plansetdetail->lead_auditor_id))
        {{ Form::select('lead_auditor_id',$auditees,$plansetdetail->lead_auditor_id,['placeholder'=>'==เลือก==','class'=>"selectpicker form-control", 'data-live-search'=>"true" ,'required'=>true]) }}
    @else
        
       {{ Form::select('lead_auditor_id',$auditees,null,['placeholder'=>'==เลือก==','class'=>"selectpicker form-control", 'data-live-search'=>"true",'required'=>true]) }} 
    @endif
   
    {!! $errors->first('lead_auditor_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group  col-6  {{ $errors->has('auditee_id') ? 'has-error' : ''}}">
    <label for="auditee_id" class="control-label">{{ 'Auditee' }}</label>
    @if (isset($plansetdetail->lead_auditor_id))
        {{ Form::select('auditee_id',$auditees,$plansetdetail->auditee_id,['placeholder'=>'==เลือก==','class'=>"selectpicker form-control", 'data-live-search'=>"true",'required'=>true]) }}
    @else
       {{ Form::select('auditee_id',$auditees,null,['placeholder'=>'==เลือก==','class'=>"selectpicker form-control", 'data-live-search'=>"true",'required'=>true]) }} 
    @endif
   
    {!! $errors->first('auditee_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group  col-6 {{ $errors->has('note') ? 'has-error' : ''}}">
    <label for="note" class="control-label">{{ 'Note' }}</label>
    <textarea class="form-control" rows="5" name="note" type="textarea" id="note" required >{{ isset($plansetdetail->note) ? $plansetdetail->note : ''}}</textarea>
    {!! $errors->first('note', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group  col-3  {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="control-label">{{ 'Status' }}</label>
    <input class="form-control" name="status" type="text" id="status" value="{{ isset($plansetdetail->status) ? $plansetdetail->status : 'Active '}}" >
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    <br/>
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
</div>