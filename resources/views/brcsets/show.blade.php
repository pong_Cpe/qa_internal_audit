@extends('layouts.theme')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">brcset {{ $brcset->id }}</div>
                <div class="card-body">

                    <a href="{{ url('/brcsets') }}" title="Back"><button class="btn btn-warning btn-sm"><i
                                class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <a href="{{ url('/brcsets/' . $brcset->id . '/edit') }}" title="Edit brcset"><button
                            class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            Edit</button></a>

                    <form method="POST" action="{{ url('brcsets' . '/' . $brcset->id) }}" accept-charset="UTF-8"
                        style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger btn-sm" title="Delete brcset"
                            onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                                aria-hidden="true"></i> Delete</button>
                    </form>
                    <br />
                    <br />

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $brcset->id }}</td>
                                </tr>
                                <tr>
                                    <th> Name </th>
                                    <td> {{ $brcset->name }} </td>
                                </tr>
                                <tr>
                                    <th> Desc </th>
                                    <td> {{ $brcset->desc }} </td>
                                </tr>
                                <tr>
                                    <th> Status </th>
                                    <td> {{ $brcset->status }} </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Seq</th>
                                    <th>กลุ่ม</th>
                                    <th>Check list</th>
                                    <th>ข้อกำหนด</th>
                                    <th>แผนกที่เกี่ยวข้อง</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @if (isset($brcgroupid))
                                    @foreach ($brcset->brctopics()->where('brc_group_id', $brcgroupid)->orderBy('seq')->get()
        as $item)

                                        <tr>
                                            <td>{{ $item->seq }}</td>
                                            <td>{{ $item->brcgroup->name }}</td>
                                            <td>{{ $item->topic }}</td>
                                            <td>{{ $item->ref }}</td>
                                            <td>

                                                @if ($item->brcdeps()->count() == $depscount)
                                                    ALL
                                                @else

                                                    @foreach ($item->brcdeps()->get() as $itemdep)
                                                        {{ $itemdep->dep->name }} ,
                                                    @endforeach

                                                @endif
                                            </td>
                                            <td><a href="{{ url('/brctopics/edittopic/' . $brcset->id . '/' . $item->id) }}"
                                                    title="View brcset"><button class="btn btn-primary btn-sm"><i
                                                            class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                        Edit</button></a>

                                                <a href="{{ url('/brctopics/deleteall/' . $item->id) }}"
                                                    title="delete brctopic"
                                                    onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                    <button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"
                                                            aria-hidden="true"></i> Delete</button>
                                                </a>
                                            </td>
                                        </tr>

                                    @endforeach
                                @else
                                    @foreach ($brcset->brctopics()->orderBy('brc_group_id')->orderBy('seq')->get()
        as $item)

                                        <tr>
                                            <td>{{ $item->seq }}</td>
                                            <td>{{ $item->brcgroup->name }}</td>
                                            <td>{{ $item->topic }}</td>
                                            <td>{{ $item->ref }}</td>
                                            <td>

                                                @if ($item->brcdeps()->count() == $depscount)
                                                    ALL
                                                @else

                                                    @foreach ($item->brcdeps()->get() as $itemdep)
                                                        {{ $itemdep->dep->name }} ,
                                                    @endforeach

                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ url('/brctopics/edittopic/' . $brcset->id . '/' . $item->id) }}"
                                                    title="View brctopic">
                                                    <button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                                            aria-hidden="true"></i> Edit</button>
                                                </a>
                                                <a href="{{ url('/brctopics/deleteall/' . $item->id) }}"
                                                    title="delete brctopic"
                                                    onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                    <button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"
                                                            aria-hidden="true"></i> Delete</button>
                                                </a>
                                            </td>
                                        </tr>

                                    @endforeach
                                @endif


                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
