@extends('layouts.theme')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h2>แผนการ Audit {{ $planset->id }}</h2></div>
                <div class="card-body">

                    <a href="{{ url('/plansets') }}" title="Back"><button class="btn btn-warning btn-sm"><i
                                class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <a href="{{ url('/plansetdetails/createwithplan/' . $planset->id) }}"
                        title="Add plansetdetail"><button class="btn btn-info btn-sm"><i class="fa fa-eye"
                                aria-hidden="true"></i> เพิ่มแผนก Audit</button></a>
                    <br />
                    <br />

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $planset->id }}</td>
                                    <th> Name </th>
                                    <td> {{ $planset->name }} </td>
                                </tr>
                                <tr>
                                    <th> Start Date </th>
                                    <td> {{ $planset->start_date }} </td>
                                    <th> End Date </th>
                                    <td> {{ $planset->end_date }} </td>
                                </tr>
                                <tr>
                                    <th> Desc </th>
                                    <td> {{ $planset->desc }} </td>
                                    <th> QMR </th>
                                    <td> {{ $planset->qmr->name }} </td>
                                </tr>
                                <tr>
                                    <th> BRC set </th>
                                    <td> {{ $planset->brcset->name }} </td>
                                    <th> Status </th>
                                    <td> {{ $planset->status }} </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Department</th>
                                    <th>Lead Auditor</th>
                                    <th>Auditor</th>
                                    <th>Auditee</th>
                                    <th>Plan / Car Plan</th>
                                    <th>Audits No.</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($planset->plansetdetails()->get() as $item)
                                    <tr>
                                        <td>{{ $item->dep->name }}</td>
                                        <td>{{ $item->leadauditor->name ?? '-' }}</td>
                                        <td>
                                            @foreach ($item->auditors()->get() as $auditorObj)
                                                {{ $auditorObj->auditor->name }}
                                                <a href="{{ url('/plansetdetailusers/deleteuser/' . $auditorObj->id) }}"
                                                    title="Delete Auditor"
                                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><button
                                                        class="btn btn-danger btn-sm"><i class="fa fa-trash-o"
                                                            aria-hidden="true"></i> Delete</button></a><br />

                                            @endforeach
                                            <br/>

                                            <a href="{{ url('/plansetdetailusers/adduser/' . $item->id) }}"
                                                title="Add Auditor"><button class="btn btn-primary btn-sm"><i
                                                        class="fa fa-pencil-square-o" aria-hidden="true"></i> เพิ่มผู้ช่วย 
                                                    Auditor</button></a>
                                        </td>
                                        <td>{{ $item->auditee->name ?? '-' }}</td>
                                        <td>{{ date('Y-m-d H:i',strtotime($item->plan_start)) }} - {{ date('Y-m-d H:i',strtotime($item->plan_end)) }} <br />/
                                            {{ date('Y-m-d H:i',strtotime($item->act_start)) }} - {{ date('Y-m-d H:i',strtotime($item->act_end)) }}</td>
                                        <td>
                                            <a href="{{ url('/audits/checklists/' . $item->id) }}"
                                                title="Edit plansetdetail">
                                                {{ $item->audits()->count() }}</a>
                                        </td>
                                        <td>{{ $item->status }}</td>
                                        <td>
                                            @if ($item->status == 'CAR')
                                            <a href="{{ url('/cars/mycarslist/' . $item->id.'/'.$item->dep->id ) }}" title="View planset"><button
                                                    class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i>
                                                    CAR</button></a>    
                                            @endif

                                            @if ($item->audits()->count() == 0)
                                                <a href="{{ url('/audits/generateAudit/' . $item->id) }}"
                                                    title="Edit plansetdetail"><button class="btn btn-primary btn-sm"><i
                                                            class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                        Generate</button></a>

                                                        <a href="{{ url('/plansetdetails/deletedetail/' . $item->id) }}"
                                                title="Delete Auditor"
                                                onclick="return confirm(&quot;Confirm delete?&quot;)"><button
                                                    class="btn btn-danger btn-sm"><i class="fa fa-trash-o"
                                                        aria-hidden="true"></i> Delete</button></a><br />

                                            @else
                                                <a href="{{ url('/audits/listbygroup/' . $item->id . '?prevurl=' . URL::current()) }}"
                                                    title="Edit plansetdetail"><button class="btn btn-success btn-sm"><i
                                                            class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                        Audit List</button></a>
                                            @endif
                                            
                                            <a href="{{ url('/plansetdetails/' . $item->id . '/edit?prevurl=' . URL::current()) }}"
                                                title="Edit plansetdetail"><button class="btn btn-primary btn-sm"><i
                                                        class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                    Edit</button></a>
                                                    
                                            


                                            





                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
