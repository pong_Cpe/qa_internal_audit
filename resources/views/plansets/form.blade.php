<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'Name' }}</label>
    <input class="form-control" required name="name" type="text" id="name" value="{{ isset($planset->name) ? $planset->name : ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc"   class="control-label">{{ 'Desc' }}</label>
    <textarea class="form-control" required rows="5" name="desc" type="textarea" id="desc" >{{ isset($planset->desc) ? $planset->desc : ''}}</textarea>
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('start_date') ? 'has-error' : ''}}">
    <label for="start_date" class="control-label">{{ 'Start Date' }}</label>
    <input class="form-control" required  name="start_date" type="date" id="start_date" value="{{ isset($planset->start_date) ? $planset->start_date : ''}}" >
    {!! $errors->first('start_date', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('end_date') ? 'has-error' : ''}}">
    <label for="end_date" class="control-label">{{ 'End Date' }}</label>
    <input class="form-control" required  name="end_date" type="date" id="end_date" value="{{ isset($planset->end_date) ? $planset->end_date : ''}}" >
    {!! $errors->first('end_date', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('brc_set_id') ? 'has-error' : ''}}">
    <label for="brc_set_id" class="control-label">{{ 'BRC set' }}</label>
    @if (isset($planset->brc_set_id))
       {{ Form::select('brc_set_id',$brcsets,$planset->brc_set_id,['placeholder'=>'==เลือก==','class'=>"form-control",'required'=>true]) }}  
    @else
       {{ Form::select('brc_set_id',$brcsets,null,['placeholder'=>'==เลือก==','class'=>"form-control",'required'=>true]) }} 
    @endif
   
    {!! $errors->first('brc_set_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('qmr_id') ? 'has-error' : ''}}">
    <label for="qmr_id" class="control-label">{{ 'QMR' }}</label>
    @if (isset($planset->qmr_id))
        {{ Form::select('qmr_id',$users,$planset->qmr_id,['placeholder'=>'==เลือก==','class'=>"form-control",'required'=>true]) }}
    @else
       {{ Form::select('qmr_id',$users,null,['placeholder'=>'==เลือก==','class'=>"form-control",'required'=>true]) }} 
    @endif
   
    {!! $errors->first('qmr_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="control-label">{{ 'Status' }}</label>
    <input class="form-control" name="status" type="text" id="status" value="{{ isset($planset->status) ? $planset->status : 'Active'}}" >
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
