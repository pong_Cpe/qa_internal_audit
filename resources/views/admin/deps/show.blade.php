@extends('layouts.theme')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">dep {{ $dep->id }}</div>
                <div class="card-body">

                    <a href="{{ url('/admin/deps') }}" title="Back"><button class="btn btn-warning btn-sm"><i
                                class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <a href="{{ url('/admin/deps/' . $dep->id . '/edit') }}" title="Edit dep"><button
                            class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            Edit</button></a>

                    <form method="POST" action="{{ url('admin/deps' . '/' . $dep->id) }}" accept-charset="UTF-8"
                        style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger btn-sm" title="Delete dep"
                            onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                                aria-hidden="true"></i> Delete</button>
                    </form>
                    <br />
                    <br />

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $dep->id }}</td>
                                </tr>
                                <tr>
                                    <th> Name </th>
                                    <td> {{ $dep->name }} </td>
                                </tr>
                                <tr>
                                    <th> Desc </th>
                                    <td> {{ $dep->desc }} </td>
                                </tr>
                                <tr>
                                    <th> Status </th>
                                    <td> {{ $dep->status }} </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
