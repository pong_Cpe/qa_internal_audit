@extends(config('laravelusers.laravelUsersBladeExtended'))

@section('template_title')
    {!! trans('laravelusers::laravelusers.showing-all-users') !!}
@endsection

@section('template_linked_css')
    @if(config('laravelusers.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('laravelusers.datatablesCssCDN') }}">
    @endif
    @if(config('laravelusers.fontAwesomeEnabled'))
        <link rel="stylesheet" type="text/css" href="{{ config('laravelusers.fontAwesomeCdn') }}">
    @endif
    @include('laravelusers::partials.styles')
    @include('laravelusers::partials.bs-visibility-css')
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Update group #{{ $user->id }}</div>
                    <div class="card-body">
                        Name : {{ $user->name }}
                        <br/>Email : {{ $user->email }}
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        
                        <form method="POST" action="{{ url('/admin/groups/updateGroupAction/'.$user->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            
                            {{ csrf_field() }}
                            <div class="form-group {{ $errors->has('group_id') ? 'has-error' : ''}}">
                                <label for="group_id" class="control-label">{{ 'Group' }}</label>
                                {{ Form::select('group_id',$groups,$user->group_id,['placeholder'=>'===เลือก===','class'=>'form-control']) }}
                                {!! $errors->first('group_id', '<p class="help-block">:message</p>') !!}
                            </div>

                                <div class="form-group">
    <input class="btn btn-primary" type="submit" value="Update">
</div>
                            </div>
                            

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
