<div class="col-md-3">
    <div class="card">
        <div class="card-header">
            Sidebar
        </div>

        <div class="card-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    <a href="{{ url('/users') }}">
                        Users
                    </a>
                    
                </li>
                <li>
                    <a href="{{ url('/admin/groups') }}">
                        Groups
                    </a>                    
                </li>
            </ul>
        </div>
    </div>
</div>
