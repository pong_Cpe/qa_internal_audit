@extends('layouts.theme')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3>เพิ่มผู้ช่วย Auditor</h3>
                </div>
                <div class="card-body">
                    <a href="{{ url('/plansets/' . $plansetdetail->plan_set_id) }}" title="Back">
                        <button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                            Back</button>
                    </a>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <div class="row">
                        <div class="col-6">
                            <h3>Plan Audit: {{ $plansetdetail->planset->name }}</h3>

                        </div>
                        <div class="col-6">
                            <h3>Plan Date: {{ $plansetdetail->planset->start_date }} - {{ $plansetdetail->planset->end_date }}</h3>

                        </div>
                    </div>    
                    <div class="row">
                        <div class="col-4">
                            <h3>Audit แผนก: {{ $plansetdetail->dep->name }}</h3>

                        </div>
                        <div class="col-4">
                            <h3>Lead Auditor: {{ $plansetdetail->leadauditor->name }}</h3>

                        </div>
                        <div class="col-4">
                            <h3>Auditee: {{ $plansetdetail->auditee->name }}</h3>

                        </div>
                    </div>
                    <br />
                    <form method="POST" action="{{ url('/plansetdetailusers/adduserAction/' . $plansetdetail->id) }}"
                        accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        @include ('plansetdetailusers.form', ['formMode' => 'create'])

                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
