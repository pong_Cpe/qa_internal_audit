<div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
    <label for="user_id" class="control-label">{{ 'Auditor' }}</label>
    @if (isset($plansetdetailuser->user_id))
        {{ Form::select('user_id',$users,$plansetdetailuser->user_id,['placeholder'=>'==เลือก==','class'=>"selectpicker form-control", 'data-live-search'=>"true"]) }}
    @else
       {{ Form::select('user_id',$users,null,['placeholder'=>'==เลือก==','class'=>"selectpicker form-control", 'data-live-search'=>"true"]) }} 
    @endif
   
    {!! $errors->first('qmr_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
    <label for="type" class="control-label">{{ 'Type' }}</label>
    <input class="form-control" name="type" type="text" id="type" value="{{ isset($plansetdetailuser->type) ? $plansetdetailuser->type : 'Auditor'}}" >
    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
