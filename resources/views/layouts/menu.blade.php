@guest
    
@else

@php
    $user = Auth::user();
    $auditorplansetdetailid = 0;
    $auditordepid = 0;
    $auditeeplansetdetailid = 0;
    $auditeedepid = 0;
    $adminflag = false;
    if($user->group->name != 'admin' && $user->group->name != 'qmr'){

        $planset = App\Models\PlanSet::where('status','Active')->first();
        //echo "planset ".$planset->id;
        foreach ($planset->plansetdetails as $plansetdetailObj) {
            //echo "Lead Auditor ".$plansetdetailObj->lead_auditor_id;
            if($plansetdetailObj->lead_auditor_id == $user->id){
                $auditorplansetdetailid = $plansetdetailObj->id;
                $auditordepid = $plansetdetailObj->dep_id;
             //   echo "Lead Auditor";
            }else{
                foreach ($plansetdetailObj->auditors as $auditorObj) {
                    if($auditorObj->user_id == $user->id){
            //            echo "Auditor";
                       $auditorplansetdetailid = $plansetdetailObj->id;
                       $auditordepid = $plansetdetailObj->dep_id;
                    }
                }  
            }

            if ($plansetdetailObj->auditee_id == $user->id) {
            //    echo "Lead Auditee";
                $auditeeplansetdetailid = $plansetdetailObj->id;
                $auditeedepid = $plansetdetailObj->dep_id;
            }

            //echo $plansetdetailObj->auditee_id ;
        }

    }else{
        if($user->group->name == 'admin' || $user->group->name == 'qmr'){
            $adminflag = true;
        }
    }
   // echo $user->id . ' ' .$auditorplansetdetailid. ' ' .$auditeeplansetdetailid. ' ' .$auditordepid.' '.$auditeedepid;
@endphp
@if ( $adminflag )
    <li>
                        <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                            <i class="icon-grid menu-icon"></i><span class="nav-text">Base Data</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a class="dropdown-item" href="{{ route('users') }}">{!! trans('laravelusers::app.nav.users') !!}</a></li>
                            <li><a class="dropdown-item" href="{{ url('/admin/groups') }}">Groups</a></li>
                        </ul>
                    </li>
                     <li>
                        <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                            <i class="icon-note menu-icon"></i><span class="nav-text">QMR</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a class="dropdown-item" href="{{ url('/brcgroups') }}">BRC Groups</a></li>
                            <li><a class="dropdown-item" href="{{ url('/brcsets') }}">BRC Set</a></li>
                            <li><a class="dropdown-item" href="{{ url('/plansets') }}">Plan Set</a></li>
                             <li><a class="dropdown-item" href="{{ url('/cars/list') }}">Cars</a></li>
                        </ul>
                    </li>
@endif

@if ($auditorplansetdetailid != 0 &&  $auditordepid != 0)
    <a class="dropdown-item" href="{{ url('/audits/listbygroup/'.$auditorplansetdetailid ) }}">Auditor</a>
    <a class="dropdown-item" href="{{ url('/cars/mycarslistforclose/'.$auditorplansetdetailid.'/'.$auditordepid) }}">Auditor Cars</a>
@endif
@if ($auditeeplansetdetailid != 0 &&  $auditeedepid != 0)
    <a class="dropdown-item" href="{{ url('/cars/mycarslist/'.$auditeeplansetdetailid.'/'.$auditeedepid) }}">Auditee Cars</a>
@endif

                                       
        
@endguest

                               