<?php

return [
    'appstatus' => 'test',
    'testemail' => [
        'to' => ['CH'=> 'Chonlada@Lannaagro.com'],
        'cc' => ['NCH' => 'Nittaya@Lannaagro.com', 'PKP' => 'parinya.k@Lannaagro.com',],
        'presubject' => '[ทดสอบ] ',
    ],
];