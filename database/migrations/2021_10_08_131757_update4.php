<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'deps',
            function (Blueprint $table) {
                $table->string('email', 100)->nullable();
                $table->string('cc_email', 100)->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('deps', 'email')) {
            Schema::table('deps', function (Blueprint $table) {
                $table->dropColumn('email');
            });
        }
        if (Schema::hasColumn('deps', 'cc_email')) {
            Schema::table('deps', function (Blueprint $table) {
                $table->dropColumn('cc_email');
            });
        }
    }
}
