<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_sets', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('desc',1000)->nullable();
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('qmr_id');
            $table->integer('brc_set_id');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_sets');
    }
}
