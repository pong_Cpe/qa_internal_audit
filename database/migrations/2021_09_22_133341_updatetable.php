<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Updatetable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'audit_chks',
            function (Blueprint $table) {
                $table->string('review',500)->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('audit_chks', 'review')) {
            Schema::table('audit_chks', function (Blueprint $table) {
                $table->dropColumn('review');
            });
        }
    }
}
