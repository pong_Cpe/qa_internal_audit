<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuditChksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_chks', function (Blueprint $table) {
            $table->id();
            $table->integer('plan_set_id');
            $table->integer('plan_detail_set_id');
            $table->integer('brc_topic_id');
            $table->integer('brc_dep_id');
            $table->date('audit_date');
            $table->string('ans');
            $table->string('note');
            $table->string('status');
            $table->string('pic')->nullable();
            $table->string('pic_path')->nullable();
            $table->integer('ans_user_id')->nullable();
            $table->integer('review_user_id')->nullable();
            $table->integer('approve_user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit_chks');
    }
}
