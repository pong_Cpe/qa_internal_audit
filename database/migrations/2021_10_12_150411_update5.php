<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update5 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'car_audits',
            function (Blueprint $table) {
                $table->string('feedbackcar', 300)->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('car_audits', 'feedbackcar')) {
            Schema::table('car_audits', function (Blueprint $table) {
                $table->dropColumn('feedbackcar');
            });
        }
    }
}
