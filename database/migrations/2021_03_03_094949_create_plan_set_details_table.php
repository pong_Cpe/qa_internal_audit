<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanSetDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_set_details', function (Blueprint $table) {
            $table->id();
            $table->integer('plan_set_id');
            $table->integer('dep_id');
            $table->datetime('plan_start');
            $table->datetime('plan_end');
            $table->datetime('act_start')->nullable();
            $table->datetime('act_end')->nullable();
            $table->string('note',1000)->nullable();
            $table->integer('lead_auditor_id');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_set_details');
    }
}
