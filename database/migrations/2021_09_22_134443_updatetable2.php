<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Updatetable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'audit_chks',
            function (Blueprint $table) {
                $table->string('reviewfile')->nullable();
                $table->string('reviewfile_path')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('audit_chks', 'reviewfile')) {
            Schema::table('audit_chks', function (Blueprint $table) {
                $table->dropColumn('reviewfile');
            });
        }
        if (Schema::hasColumn('audit_chks', 'reviewfile_path')) {
            Schema::table('audit_chks', function (Blueprint $table) {
                $table->dropColumn('reviewfile_path');
            });
        }
    }
}
