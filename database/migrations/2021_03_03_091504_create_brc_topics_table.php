<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrcTopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brc_topics', function (Blueprint $table) {
            $table->id();
            $table->integer('brc_set_id');
            $table->integer('brc_group_id');
            $table->integer('seq')->nullable();
            $table->string('ref');
            $table->string('topic',1000);
            $table->string('type');
            $table->text('desc')->nullable();
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brc_topics');
    }
}
