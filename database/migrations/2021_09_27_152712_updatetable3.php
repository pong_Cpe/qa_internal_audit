<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Updatetable3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'car_sets',
            function (Blueprint $table) {
                $table->datetime('returncar_date')->nullable();
                $table->string('returncar_note',500)->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('car_sets', 'returncar_date')) {
            Schema::table('car_sets', function (Blueprint $table) {
                $table->dropColumn('returncar_date');
            });
        }

        if (Schema::hasColumn('car_sets', 'returncar_note')) {
            Schema::table('car_sets', function (Blueprint $table) {
                $table->dropColumn('returncar_note');
            });
        }
    }
}
