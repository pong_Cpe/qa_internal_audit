<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_audits', function (Blueprint $table) {
            $table->id();
            $table->integer('car_set_id');
            $table->integer('audit_chk_id');
            $table->date('car_date');
            $table->string('prevention')->nullable();
            $table->string('correction')->nullable();
            $table->string('corrective_action')->nullable();
            $table->string('status');
            $table->string('pic1')->nullable();
            $table->string('pic1_path')->nullable();
            $table->string('pic2')->nullable();
            $table->string('pic2_path')->nullable();
            $table->string('pic3')->nullable();
            $table->string('pic3_path')->nullable();
            $table->integer('ans_user_id')->nullable();
            $table->integer('review_user_id')->nullable();
            $table->integer('approve_user_id')->nullable();
            $table->integer('ref_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_audits');
    }
}
