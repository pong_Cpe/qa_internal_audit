<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Send2Auditor extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $mailObj;

    public function __construct($obj)
    {
        $this->mailObj = $obj;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mailObj = $this->mailObj;
        if (config('myconfig.appstatus') == 'test') {
            return $this->view('emails.sendtoauditor', compact('mailObj'))->subject(config('myconfig.testemail.presubject') . $this->mailObj['subject']);
        } else {
            return $this->view('emails.sendtoauditor', compact('mailObj'))->subject($this->mailObj['subject']);
        } 
        
    }
}
