<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class SendGeneratePlan extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($obj)
    {
        $this->mailObj = $obj;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        $mailObj = $this->mailObj;
        $filewithpath = Storage::path($mailObj['linkics']);
        if(config('myconfig.appstatus') == 'test'){
            return $this->view('emails.sentgenerateplandep', compact('mailObj'))->subject(config('myconfig.testemail.presubject').$this->mailObj['subject'])->attach($filewithpath);
        }else{
            return $this->view('emails.sentgenerateplandep', compact('mailObj'))->subject($this->mailObj['subject']);
        }        
    }
}
