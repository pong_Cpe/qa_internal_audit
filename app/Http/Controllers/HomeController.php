<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\PlanSet;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        if($user->group->name == 'admin'){
            return redirect('brcsets');
        }elseif($user->group->name == 'qmr'){
            return redirect('plansets');
        }else{

            $planset = PlanSet::where('status','Active')->first();

            foreach ($planset->plansetdetails as $plansetdetailObj) {
                if($plansetdetailObj->lead_auditor_id == $user->id){
                    return redirect('/audits/listbygroup/' . $plansetdetailObj->id);
                }else{
                    foreach ($plansetdetailObj->auditors as $auditorObj) {
                        if($auditorObj->user_id == $user->id){
                            return redirect('/audits/listbygroup/' . $plansetdetailObj->id);
                        }
                    }

                    if ($plansetdetailObj->auditee_id == $user->id) {

                        return redirect('/cars/mycarslist/' . $plansetdetailObj->id."/". $plansetdetailObj->dep_id);
                    }
                }
            }

            return redirect('/plansets/'.$planset->id);
        }
        
    }
}
