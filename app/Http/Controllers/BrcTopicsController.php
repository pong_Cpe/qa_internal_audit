<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\BrcTopic;
use App\Models\BrcSet;
use App\Models\BrcGroup;
use App\Models\BrcDep;
use App\Models\Dep;
use Illuminate\Http\Request;

class BrcTopicsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $brctopics = BrcTopic::latest()->paginate($perPage);
        } else {
            $brctopics = BrcTopic::latest()->paginate($perPage);
        }

        return view('brctopics.index', compact('brctopics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $brcsets = BrcSet::pluck('name', 'id');
        $brcgroups = BrcGroup::pluck('name', 'id');
        return view('brctopics.create',compact('brcsets', 'brcgroups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        BrcTopic::create($requestData);

        return redirect('brctopics')->with('flash_message', 'BrcTopic added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $brctopic = BrcTopic::findOrFail($id);

        return view('brctopics.show', compact('brctopic'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $brctopic = BrcTopic::findOrFail($id);
        $brcsets = BrcSet::pluck('name', 'id');
        $brcgroups = BrcGroup::pluck('name', 'id');

        return view('brctopics.edit', compact('brctopic', 'brcsets', 'brcgroups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $brctopic = BrcTopic::findOrFail($id);
        $brctopic->update($requestData);

        return redirect('brctopics')->with('flash_message', 'BrcTopic updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        BrcTopic::destroy($id);

        return redirect('brctopics')->with('flash_message', 'BrcTopic deleted!');
    }

    public function addtopic($brcid)
    {
        $brcsets = BrcSet::pluck('name', 'id');
        $brcgroups = BrcGroup::pluck('name', 'id');
        $deps = Dep::pluck('name', 'id');
        return view('brctopics.addtopic', compact('brcsets', 'brcgroups', 'brcid', 'deps'));
    }

    public function addtopicAction(Request $request, $brcid)
    {

        $requestData = $request->all();

        $brctopic = BrcTopic::create($requestData);

        if(isset($requestData['dep_id'])){
            foreach ($requestData['dep_id'] as $depid) {
                $tmp = array();
                $tmp['brc_topic_id'] = $brctopic->id;
                $tmp['brc_set_id'] = $brctopic->brc_set_id;
                $tmp['dep_id'] = $depid;
                BrcDep::create($tmp);
            }
        }


        return redirect('brcsets')->with('flash_message', 'BrcTopic added!');
    }

    public function edittopic($brcsetid,$detailid)
    {
        $brctopic = BrcTopic::findOrFail($detailid);
        $brcsets = BrcSet::pluck('name', 'id');
        $brcgroups = BrcGroup::pluck('name', 'id');
        $deps = Dep::pluck('name', 'id');
        $deptopics = array();
        foreach ($brctopic->brcdeps()->get() as $brcdepObj) {
            $deptopics[$brcdepObj->dep_id] = $brcdepObj;
        }

        return view('brctopics.edittopic', compact('brctopic', 'brcsets', 'brcgroups', 'brcsetid', 'deps', 'deptopics'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edittopicAction(Request $request, $brcsetid,$detailid)
    {

        $requestData = $request->all();

        $brctopic = BrcTopic::findOrFail($detailid);
        $brctopic->update($requestData);

        BrcDep::where('brc_topic_id', $detailid)->delete();

        if (isset($requestData['dep_id'])) {
            foreach ($requestData['dep_id'] as $depid) {                
                $tmp = array();
                $tmp['brc_topic_id'] = $detailid;
                $tmp['brc_set_id'] = $brctopic->brc_set_id;
                $tmp['dep_id'] = $depid;
                BrcDep::create($tmp);
            }
        }

        return redirect('brcsets/'. $brcsetid)->with('flash_message', 'BrcTopic updated!');
    }

    public function deleteall($id)
    {

        $brctopic = BrcTopic::findOrFail($id);

        $brcsetid = $brctopic->brc_set_id;
        $brcgroupid = $brctopic->brc_group_id;

        BrcDep::where('brc_topic_id', $id)->delete();

        BrcTopic::destroy($id);

        return redirect('brcsets/showwithgroup/'. $brcsetid .'/'. $brcgroupid)->with('flash_message', 'BrcTopic deleted!');
    }
}
