<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

use App\Models\AuditChk;
use App\Models\BrcDep;
use App\Models\BrcGroup;
use App\Models\PlanSet;
use App\Models\PlanSetDetail;
use App\Models\BrcTopic;
use App\Models\PlanSetDetailUser;
use App\Models\CarSet;
use App\Mail\Send2Auditee;
use App\Mail\SendGeneratePlan;
use Illuminate\Http\Request;

use Spatie\CalendarLinks\Link;
use DateTime;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;

class AuditsController extends Controller
{
    public function __construct()
    {
        ini_set('memory_limit', '-1');
    }

    public function generateAudit($planSetDetailId)
    {



        $plansetdetail = PlanSetDetail::findOrFail($planSetDetailId);


        $brcdeps = BrcDep::where('brc_set_id', $plansetdetail->brc_set_id)->where('dep_id', $plansetdetail->dep_id)->get();

        $from = new DateTime($plansetdetail->plan_start);
        $to = new DateTime($plansetdetail->plan_end);



        $link = Link::create("แผนการ Audit '".$plansetdetail->planset->name."' ของแผนก ". $plansetdetail->dep->name, $from, $to)
        ->description($plansetdetail->planset->desc)
        ->address('LACO');

        $linkplan = '<a href="' . $link->ics() . '" class="ics-link">Download My Event</a>';

        //Send Email to auditee

        //$ftStaff = config('myconfig.emaillist');

        $mailObj = array();

        $mailObj['plansetdetail'] = $plansetdetail;
        $mailObj['subject'] = "[QA Internal Audit] แผนการ Audit " . $plansetdetail->dep->name;
        $mailObj['linkics'] = 'audit-'. $plansetdetail->id .'.ics';

       // $encodedData = str_replace(' ', '+', $link->ics());
       // $decodedData = base64_decode($encodedData);
        $imgData = str_replace(' ', '+', $link->ics());
        $imgData =  substr($imgData, strpos($imgData, ",") + 1);
        $imgData = base64_decode($imgData);

        Storage::put($mailObj['linkics'], $imgData);

        if (config('myconfig.appstatus') == 'test') {
            Mail::to([
                "PKP" => 'parinya.k@lannaagro.com',
            ])->cc([
                "PKP" => 'parinya.k@lannaagro.com',
            ])->send(new SendGeneratePlan($mailObj));
        }else{
            Mail::to([
                $plansetdetail->dep->name => $plansetdetail->dep->email,
            ])->cc([
                $plansetdetail->dep->cc_email => $plansetdetail->dep->cc_email,
            ])->send(new SendGeneratePlan($mailObj));
        }

        foreach ($brcdeps as $brcdepObj) {

            $chk = AuditChk::where('plan_detail_set_id', $planSetDetailId)->where('brc_topic_id', $brcdepObj->brc_topic_id)->where('brc_dep_id', $brcdepObj->dep_id)->count();

            if ($chk == 0) {
                $tmpAuditChk = array();

                $tmpAuditChk['plan_set_id'] = $plansetdetail->plan_set_id;
                $tmpAuditChk['plan_detail_set_id'] = $plansetdetail->id;
                $tmpAuditChk['brc_topic_id'] = $brcdepObj->brc_topic_id;
                $tmpAuditChk['brc_dep_id'] = $brcdepObj->dep_id;
                $tmpAuditChk['brc_group_id'] = $brcdepObj->brctopic->brc_group_id;
                $tmpAuditChk['status'] = 'Created';

                AuditChk::create($tmpAuditChk);
            }
        }

        return redirect('plansets/' . $plansetdetail->plan_set_id)->with('flash_message', 'PlanSetDetail updated!');
    }

    public function checklists($planSetDetailId)
    {

        $user = Auth::user();

        $checklistans = array(
            'Y' => 'มีการปฏิบัติสอดคล้องกับข้อกำหนด',
            'N' => 'ไม่มีการนำไปปฏิบัติตามข้อกำหนด',
            'Ob' => 'ข้อเสนอแนะเพื่อปรับปรุง'
        );

        $checklistcar = array(
            'critical' => 'Critical CAR',
            'major' => 'Major CAR',
            'minor' => 'Minor CAR',
            'observation' => 'Observation',
            'off' => 'Off record'
        );

        $plansetdetail = PlanSetDetail::findOrFail($planSetDetailId);

        $auditChks = AuditChk::where('plan_detail_set_id', $planSetDetailId)->get();

        return view('audits.checklists', compact('plansetdetail', 'auditChks', 'checklistans', 'checklistcar'));
    }

    public function checklistsAction(Request $request, $planSetDetailId)
    {
        $requestData = $request->all();
        $user = Auth::user();
        $auditChks = AuditChk::where('plan_detail_set_id', $planSetDetailId)->get();
        foreach ($auditChks as $auditChkObj) {
            if (isset($requestData['auditchk_ans_' . $auditChkObj->id]) && !empty($requestData['auditchk_ans_' . $auditChkObj->id])) {
                $auditChkObj->ans = $requestData['auditchk_ans_' . $auditChkObj->id];
                $auditChkObj->status = 'Process';
                $auditChkObj->ans_user_id = $user->id;
            }

            if (isset($requestData['auditchk_note_' . $auditChkObj->id]) && !empty($requestData['auditchk_note_' . $auditChkObj->id])) {
                $auditChkObj->note = $requestData['auditchk_note_' . $auditChkObj->id];
                $auditChkObj->status = 'Process';
                $auditChkObj->ans_user_id = $user->id;
            }

            if (isset($requestData['auditchk_car_type_' . $auditChkObj->id]) && !empty($requestData['auditchk_car_type_' . $auditChkObj->id])) {
                $auditChkObj->car_type = $requestData['auditchk_car_type_' . $auditChkObj->id];
                $auditChkObj->status = 'Process';
                $auditChkObj->ans_user_id = $user->id;
            }

            if (isset($requestData['remove_path_' . $auditChkObj->id]) && !empty($requestData['remove_path_' . $auditChkObj->id])) {
                $auditChkObj->pic = null;
                $auditChkObj->pic_path = null;
                $auditChkObj->status = 'Process';
                $auditChkObj->ans_user_id = $user->id;
            }
            if ($request->hasFile('auditchk_pic_' . $auditChkObj->id)) {
                $image = $request->file('auditchk_pic_' . $auditChkObj->id);
                $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('images/' . $planSetDetailId);
                $image->move($destinationPath, $name);

                $auditChkObj->pic = $name;
                $auditChkObj->pic_path = 'images/' . $planSetDetailId  . "/" . $name;
                $auditChkObj->status = 'Process';
                $auditChkObj->ans_user_id = $user->id;
            }

            //Check all complete
            //fundamental case
            if ($auditChkObj->brctopic->type == 'เป็น') {
                if (!empty($auditChkObj->ans) && !empty($auditChkObj->note) && !empty($auditChkObj->car_type)) {
                    $auditChkObj->status = 'done';
                }
            } else {
                //Case N / ob
                if (!empty($auditChkObj->ans)) {
                    if (($auditChkObj->ans == 'N' || $auditChkObj->ans == 'ob') && !empty($auditChkObj->note) && !empty($auditChkObj->car_type)) {
                        $auditChkObj->status = 'done';
                    } else {
                        if (!empty($auditChkObj->car_type)) {
                            $auditChkObj->status = 'done';
                        }
                    }
                }
            }

            $auditChkObj->update();
           // dd($auditChkObj);
        }

        return redirect('audits/checklists/' . $planSetDetailId)->with('flash_message', 'PlanSetDetail updated!');
    }

    public function list()
    {
        $user = Auth::user();

        $perPage = 25;

        $planSetDetailObj = new PlanSetDetail();
        $planSetDetailObj = $planSetDetailObj->where('status', 'Active');

        if (
            $user->group->name == 'qmr'
            || $user->group->name == 'admin'
        ) {
            //echo 'all';
            // $plansetdetails = $planSetDetailObj->get();
        } else {

            $plansetdetaillist = PlanSetDetailUser::where('user_id', $user->id)
                ->pluck('id', 'id');


            $planSetDetailObj = $planSetDetailObj->Where('lead_auditor_id', $user->id);
            // /dd($planSetDetailObj); 
            if (!empty($plansetdetaillist)) {
                $planSetDetailObj = $planSetDetailObj->orWhereIn('id', $plansetdetaillist);
            }
        }

        $plansetdetails = $planSetDetailObj->latest()->paginate($perPage);



        return view('audits.list', compact('plansetdetails'));
    }

    public function listbygroup($plansetdetail_id)
    {



        // dd($link);

        $plansetdetail = PlanSetDetail::findOrFail($plansetdetail_id);

        $auditListRaw = AuditChk::where('plan_detail_set_id', $plansetdetail_id)->get();


        $from = new DateTime($plansetdetail->plan_start);
        $to = new DateTime($plansetdetail->plan_end);

        // dd($from);

        /* $link = Link::create($plansetdetail->planset->name . " Dep:  " . $plansetdetail->dep->name, $from, $to)
            ->description("Planset : " . $plansetdetail->planset->name . " - Department : " . $plansetdetail->dep->name . ' Lead Auditor : ' . $plansetdetail->leadauditor->name)
            ->address('LACO'); */

        $auditByGroup = array();
        $auditByall = array();
        foreach ($auditListRaw as $auditListObj) {
            $auditByGroup[$auditListObj->brcgroup->name]['plan_detail_set_id'] = $auditListObj->plan_detail_set_id;
            $auditByGroup[$auditListObj->brcgroup->name]['brc_group_id'] = $auditListObj->brc_group_id;
            $auditByGroup[$auditListObj->brcgroup->name]['all'][] = $auditListObj;
            //var_dump($auditListObj->ans);
            if(!empty($auditListObj->ans) || trim($auditListObj->ans) == 'N' || trim($auditListObj->ans) == 'Ob' ){
                $auditByGroup[$auditListObj->brcgroup->name]['done'][] = $auditListObj;

                $auditByall['done'][] = $auditListObj;

            }
        }
        //dd($auditByGroup);
        return view('audits.listbygroup', compact('auditByGroup', 'plansetdetail',  'auditByall'));
    }

    public function checklistsbygroup($planSetDetailId, $brc_group_id)
    {

        $brcgroupdata = BrcGroup::findOrFail($brc_group_id);

        //dd($brcgroup);

        $user = Auth::user();

        $checklistans = array(
            'Y' => 'มีการปฏิบัติสอดคล้องกับข้อกำหนด',
            'N' => 'ไม่มีการนำไปปฏิบัติตามข้อกำหนด',
            'Ob' => 'ข้อเสนอแนะเพื่อปรับปรุง'
        );

        $checklistcar = array(
            'critical' => 'Critical CAR',
            'major' => 'Major CAR',
            'minor' => 'Minor CAR',
            'observation' => 'Observation',
            'off' => 'Off record'
        );

        $plansetdetail = PlanSetDetail::findOrFail($planSetDetailId);

        $auditChks = AuditChk::where('plan_detail_set_id', $planSetDetailId)->where('brc_group_id', $brc_group_id)->get();

        return view('audits.checklistsbygroup', compact('plansetdetail', 'auditChks', 'checklistans', 'brcgroupdata', 'checklistcar'));
    }

    public function checklistsbygroupAction(Request $request, $planSetDetailId, $brc_group_id)
    {
        $requestData = $request->all();
        $user = Auth::user();
        $auditChks = AuditChk::where('plan_detail_set_id', $planSetDetailId)->where('brc_group_id', $brc_group_id)->get();
        foreach ($auditChks as $auditChkObj) {
            if (isset($requestData['auditchk_ans_' . $auditChkObj->id]) && !empty($requestData['auditchk_ans_' . $auditChkObj->id])) {
                $auditChkObj->ans = $requestData['auditchk_ans_' . $auditChkObj->id];
                $auditChkObj->status = 'Process';
                $auditChkObj->ans_user_id = $user->id;
            }

            if (isset($requestData['auditchk_note_' . $auditChkObj->id]) && !empty($requestData['auditchk_note_' . $auditChkObj->id])) {
                $auditChkObj->note = $requestData['auditchk_note_' . $auditChkObj->id];
                $auditChkObj->status = 'Process';
                $auditChkObj->ans_user_id = $user->id;
            }

            if (isset($requestData['auditchk_car_type_' . $auditChkObj->id]) && !empty($requestData['auditchk_car_type_' . $auditChkObj->id])) {
                $auditChkObj->car_type = $requestData['auditchk_car_type_' . $auditChkObj->id];
                $auditChkObj->status = 'Process';
                $auditChkObj->ans_user_id = $user->id;
            }

            if (isset($requestData['remove_path_' . $auditChkObj->id]) && !empty($requestData['remove_path_' . $auditChkObj->id])) {
                $auditChkObj->pic = null;
                $auditChkObj->pic_path = null;
                $auditChkObj->status = 'Process';
                $auditChkObj->ans_user_id = $user->id;
            }
            if ($request->hasFile('auditchk_pic_' . $auditChkObj->id)) {
                $image = $request->file('auditchk_pic_' . $auditChkObj->id);
                $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('images/' . $planSetDetailId);
                $image->move($destinationPath, $name);

                $auditChkObj->pic = $name;
                $auditChkObj->pic_path = 'images/' . $planSetDetailId  . "/" . $name;
                $auditChkObj->status = 'Process';
                $auditChkObj->ans_user_id = $user->id;
            }

            //Check all complete
            //fundamental case
            if($auditChkObj->brctopic->type == 'เป็น'){
                if(!empty($auditChkObj->ans) && !empty($auditChkObj->note) && !empty($auditChkObj->car_type)){
                    $auditChkObj->status = 'done';
                } 
            } else {
                //Case N / ob
                if (!empty($auditChkObj->ans)) {
                    if(($auditChkObj->ans == 'N' ||$auditChkObj->ans == 'ob') && !empty($auditChkObj->note) && !empty($auditChkObj->car_type)){
                        $auditChkObj->status = 'done';
                    }else{
                        if (!empty($auditChkObj->car_type)) {
                            $auditChkObj->status = 'done';
                        }
                    }
                } 
            }


            $auditChkObj->update();
        }

        return redirect('audits/listbygroup/' . $planSetDetailId)->with('flash_message', 'PlanSetDetail updated!');
    }

    public function reportaudit($planSetDetailId)
    {
        $user = Auth::user();

        $checklistans = array(
            'Y' => 'มีการปฏิบัติสอดคล้องกับข้อกำหนด',
            'N' => 'ไม่มีการนำไปปฏิบัติตามข้อกำหนด',
            'Ob' => 'ข้อเสนอแนะเพื่อปรับปรุง'
        );

        $checklistcar = array(
            'critical' => 'Critical CAR',
            'major' => 'Major CAR',
            'minor' => 'Minor CAR',
            'observation' => 'Observation',
            'off' => 'Off record'
        );

        $plansetdetail = PlanSetDetail::findOrFail($planSetDetailId);

        $auditChks = AuditChk::where('plan_detail_set_id', $planSetDetailId)->get();

        return view('audits.reportaudit', compact('plansetdetail', 'auditChks', 'checklistans'));
    }

    public function reportauditXLXS($planSetDetailId)
    {

        $plansetdetail = PlanSetDetail::findOrFail($planSetDetailId);

        $user = Auth::user();

        $checklistans = array(
            'Y' => 'มีการปฏิบัติสอดคล้องกับข้อกำหนด',
            'N' => 'ไม่มีการนำไปปฏิบัติตามข้อกำหนด',
            'Ob' => 'ข้อเสนอแนะเพื่อปรับปรุง'
        );

        $checklistcar = array(
            'critical' => 'Critical CAR',
            'major' => 'Major CAR',
            'minor' => 'Minor CAR',
            'observation' => 'Observation',
            'off' => 'Off record'
        );

        $auditChks = AuditChk::where('plan_detail_set_id', $planSetDetailId)->get();


        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValueByColumnAndRow(1, 1, "ผลการ Audit ");
        $sheet->mergeCellsByColumnAndRow(1, 1, 13, 1);
        $sheet->setCellValueByColumnAndRow(1, 2, "แผนก " . $plansetdetail->dep->name);
        $sheet->setCellValueByColumnAndRow(2, 2, "Lead Auditor ".$plansetdetail->leadauditor->name);
        $auditors = "";
        foreach($plansetdetail->auditors()->get() as $auditorObj) {
            $auditors .= $auditorObj->auditor->name.",";
        }
        $sheet->setCellValueByColumnAndRow(3, 2, "Auditor " . $auditors);
        $sheet->setCellValueByColumnAndRow(4, 2, "Auditee " . $plansetdetail->auditee->name);
        $sheet->setCellValueByColumnAndRow(5, 2, "Plan  " . $plansetdetail->plan_start ." - ".$plansetdetail->plan_end);
        $sheet->mergeCellsByColumnAndRow(5, 2, 6, 2);
        $sheet->setCellValueByColumnAndRow(7, 2, "Act  " . $plansetdetail->act_start . " - " . $plansetdetail->act_end);
        $sheet->mergeCellsByColumnAndRow(7, 2, 8, 2);

        $sheet->setCellValueByColumnAndRow(1, 3, "Check list");
        $sheet->mergeCellsByColumnAndRow(1, 3, 3, 3);
        $sheet->setCellValueByColumnAndRow(4, 3, "ข้อกำหนด");
        $sheet->mergeCellsByColumnAndRow(4, 3, 7, 3);
        $sheet->setCellValueByColumnAndRow(8, 3,"ผลการตรวจ");
        $sheet->mergeCellsByColumnAndRow(8, 3, 10, 3);
        $sheet->setCellValueByColumnAndRow(11, 3, "Note");
        $sheet->mergeCellsByColumnAndRow(11, 3, 13, 3);

        $startrow = 4;
        $runrow = $startrow;
        foreach ($auditChks as $item) {
            $sheet->setCellValueByColumnAndRow(1, $runrow, $item->brctopic->topic);
            $sheet->mergeCellsByColumnAndRow(1, $runrow, 3, $runrow);
            $sheet->setCellValueByColumnAndRow(4, $runrow, $item->brctopic->ref);
            $sheet->mergeCellsByColumnAndRow(4, $runrow, 7, $runrow);
            if(isset($checklistans[$item->ans])){
                $sheet->setCellValueByColumnAndRow(8, $runrow, $checklistans[$item->ans]);
            }
            $sheet->mergeCellsByColumnAndRow(8, $runrow, 10, $runrow);
            $sheet->setCellValueByColumnAndRow(11, $runrow, $item->note);
            $sheet->mergeCellsByColumnAndRow(11, $runrow, 13, $runrow);
            $runrow++;
        }

        $writer = new Xlsx($spreadsheet);

        $filename = "exportaudit-".date('yymmdd-hi').".xlsx";

        $writer->save('storage/'.$filename);

        return response()->download('storage/' . $filename); 
        

    //  /   return view('audits.reportaudit', compact('plansetdetail', 'auditChks', 'checklistans'));
    }

    public function checklistshows($planSetDetailId)
    {

        $user = Auth::user();

        $checklistans = array(
            'Y' => 'มีการปฏิบัติสอดคล้องกับข้อกำหนด',
            'N' => 'ไม่มีการนำไปปฏิบัติตามข้อกำหนด',
            'Ob' => 'ข้อเสนอแนะเพื่อปรับปรุง'
        );

        $checklistcar = array(
            'critical' => 'Critical CAR',
            'major' => 'Major CAR',
            'minor' => 'Minor CAR',
            'observation' => 'Observation',
            'off' => 'Off record'
        );

        $plansetdetail = PlanSetDetail::findOrFail($planSetDetailId);

        $auditChks = AuditChk::where('plan_detail_set_id', $planSetDetailId)->where('brc_dep_id', $plansetdetail->dep->id)->get();

        //dd($auditChks->count());

        return view('audits.checklistshows', compact('plansetdetail', 'auditChks', 'checklistans', 'checklistcar'));
    }

    public function changestatus($planSetDetailId,$status){
        $plansetdetail = PlanSetDetail::findOrFail($planSetDetailId);
        $plansetdetail->status = $status;
        $plansetdetail->update();

       
    }

    public function sendforclosecar($planSetDetailId){
        $plansetdetail = PlanSetDetail::findOrFail($planSetDetailId);

        return view('audits.sendforclosecar',compact('plansetdetail'));
    }

    public function sendforclosecarAction(Request $request, $planSetDetailId)
    {
        $requestData = $request->all();

        $plansetdetail = PlanSetDetail::findOrFail($planSetDetailId);

        $checkSet = CarSet::where('plan_detail_set_id', $planSetDetailId)->latest()->first();
        
        $tmpCarSet = array();
        $tmpCarSet['plan_detail_set_id'] = $planSetDetailId;
        $tmpCarSet['dep_id'] = $plansetdetail->dep_id;
        
        $tmpCarSet['deathline'] = \Carbon\Carbon::parse($requestData['deathline'])->format('Y-m-d H:i');;
        $tmpCarSet['round'] = 1;
        $tmpCarSet['note'] = $requestData['note'];
        $tmpCarSet['status'] = 'New';
        if(!empty($checkSet)){
            $tmpCarSet['round'] = $checkSet->round + 1;
        }
        $carsetnew = CarSet::create($tmpCarSet);

        //Send Email to auditee

        //$ftStaff = config('myconfig.emaillist');

        $mailObj = array();
        
        $mailObj['plansetdetail'] = $plansetdetail;
        $mailObj['carset'] = $carsetnew;
        $mailObj['subject'] = "[QA Internal Audit] Car แผนก ". $plansetdetail->dep->name;
        if (config('myconfig.appstatus') == 'test') {
            Mail::to(config('myconfig.testemail.to'))
            ->cc(config('myconfig.testemail.cc'))
            ->send(new Send2Auditee($mailObj));
        }else{
            Mail::to([
                $plansetdetail->dep->email => $plansetdetail->dep->email
            ])->cc([
                $plansetdetail->dep->cc_email => $plansetdetail->dep->cc_email
            ])->send(new Send2Auditee($mailObj));
        }
        

        $plansetdetail->status = "CAR";
        $plansetdetail->update();
        return redirect('/audits/listbygroup/'. $plansetdetail->id);
    }

    public function notelistsbygroup($planSetDetailId, $brc_group_id)
    {

        $brcgroupdata = BrcGroup::findOrFail($brc_group_id);

        //dd($brcgroup);

        $user = Auth::user();

        $checklistans = array(
            'Y' => 'มีการปฏิบัติสอดคล้องกับข้อกำหนด',
            'N' => 'ไม่มีการนำไปปฏิบัติตามข้อกำหนด',
            'Ob' => 'ข้อเสนอแนะเพื่อปรับปรุง'
        );

        $checklistcar = array(
            'critical' => 'Critical CAR',
            'major' => 'Major CAR',
            'minor' => 'Minor CAR',
            'observation' => 'Observation',
            'off' => 'Off record'
        );

        $plansetdetail = PlanSetDetail::findOrFail($planSetDetailId);

        $auditChks = AuditChk::where('plan_detail_set_id', $planSetDetailId)->where('brc_group_id', $brc_group_id)->get();

        return view('audits.notelistsbygroup', compact('plansetdetail', 'auditChks', 'checklistans', 'brcgroupdata', 'checklistcar'));
    }

    public function notelistsbygroupAction(Request $request, $planSetDetailId, $brc_group_id)
    {
        $requestData = $request->all();
        $user = Auth::user();
        $auditChks = AuditChk::where('plan_detail_set_id', $planSetDetailId)->where('brc_group_id', $brc_group_id)->get();
        foreach ($auditChks as $auditChkObj) {
            if (isset($requestData['auditchk_review_' . $auditChkObj->id]) && !empty($requestData['auditchk_review_' . $auditChkObj->id])) {
                $auditChkObj->review = $requestData['auditchk_review_' . $auditChkObj->id];
                $auditChkObj->status = 'Prepare';
                $auditChkObj->ans_user_id = $user->id;
            }

            if (isset($requestData['remove_reviewfile_' . $auditChkObj->id]) && !empty($requestData['remove_reviewfile_' . $auditChkObj->id])) {
                $auditChkObj->reviewfile = null;
                $auditChkObj->reviewfile_path = null;
                $auditChkObj->status = 'Prepare';
                $auditChkObj->ans_user_id = $user->id;
            }
            if ($request->hasFile('auditchk_reviewfile_' . $auditChkObj->id)) {
                $image = $request->file('auditchk_reviewfile_' . $auditChkObj->id);
                $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('files/' . $planSetDetailId);
                $image->move($destinationPath, $name);

                $auditChkObj->reviewfile = $image->getClientOriginalName();
                $auditChkObj->reviewfile_path = 'files/' . $planSetDetailId  . "/" . $name;
                $auditChkObj->status = 'Prepare';
                $auditChkObj->ans_user_id = $user->id;
            }
            $auditChkObj->update();
        }

        return redirect('audits/listbygroup/' . $planSetDetailId)->with('flash_message', 'PlanSetDetail updated!');
    }

    public function checkcarlistshows($planSetDetailId)
    {

        $user = Auth::user();

        $checklistans = array(
            'Y' => 'มีการปฏิบัติสอดคล้องกับข้อกำหนด',
            'N' => 'ไม่มีการนำไปปฏิบัติตามข้อกำหนด',
            'Ob' => 'ข้อเสนอแนะเพื่อปรับปรุง'
        );

        $checklistcar = array(
            'critical' => 'Critical CAR',
            'major' => 'Major CAR',
            'minor' => 'Minor CAR',
            'observation' => 'Observation',
            'off' => 'Off record'
        );

        $plansetdetail = PlanSetDetail::findOrFail($planSetDetailId);

        $auditChks = AuditChk::where('plan_detail_set_id', $planSetDetailId)
        ->where('brc_dep_id', $plansetdetail->dep->id)
        ->where('ans', '!=', 'Y')
        ->where('ans', '<>', '')
        ->get();

        $carsetno = CarSet::where('plan_detail_set_id', $planSetDetailId)->count();

        //dd($auditChks->count());

        return view('audits.checkcarlistshows', compact('plansetdetail', 'auditChks', 'checklistans', 'checklistcar', 'carsetno'));
    }
}
