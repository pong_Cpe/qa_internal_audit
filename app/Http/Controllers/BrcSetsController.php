<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\BrcSet;
use App\Models\Dep;
use App\Models\BrcGroup;
use Illuminate\Http\Request;

class BrcSetsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        
        $keyword = $request->get('search');
        $perPage = 25;

        $brcgroups = BrcGroup::where('status','Active')->pluck('name','id');

        if (!empty($keyword)) {
            $brcsets = BrcSet::latest()->paginate($perPage);
        } else {
            $brcsets = BrcSet::latest()->paginate($perPage);
        }

        return view('brcsets.index', compact('brcsets', 'brcgroups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('brcsets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        BrcSet::create($requestData);

        return redirect('brcsets')->with('flash_message', 'BrcSet added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $brcset = BrcSet::findOrFail($id);
        $depscount = Dep::where('status','Active')->count();

        return view('brcsets.show', compact('brcset', 'depscount'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $brcset = BrcSet::findOrFail($id);

        return view('brcsets.edit', compact('brcset'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $brcset = BrcSet::findOrFail($id);
        $brcset->update($requestData);

        return redirect('brcsets')->with('flash_message', 'BrcSet updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        BrcSet::destroy($id);

        return redirect('brcsets')->with('flash_message', 'BrcSet deleted!');
    }

    public function showwithgroup($brcsetid,$brcgroupid)
    {
        $brcset = BrcSet::findOrFail($brcsetid);
        $depscount = Dep::where('status', 'Active')->count();

        return view('brcsets.show', compact('brcset', 'depscount', 'brcgroupid'));
    }
}
