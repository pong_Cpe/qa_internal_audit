<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\PlanSetDetailUser;
use App\Models\PlanSetDetail;
use App\Models\User;
use Illuminate\Http\Request;

class PlanSetDetailUsesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $plansetdetailusers = PlanSetDetailUser::latest()->paginate($perPage);
        } else {
            $plansetdetailusers = PlanSetDetailUser::latest()->paginate($perPage);
        }

        return view('plansetdetailusers.index', compact('plansetdetailusers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $plansetdetailRws = PlanSetDetail::where('status', 'Active')->get();
        $plansetdetails = array();
        foreach ($plansetdetailRws as $plansetdetailObj) {
            $plansetdetails[$plansetdetailObj->id] = $plansetdetailObj->planset->name .' - '. $plansetdetailObj->dep->name;
        }

        $users = User::whereIn('group_id', array(3, 4))->pluck('name', 'id');
        return view('plansetdetailusers.create',compact('plansetdetails', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        PlanSetDetailUser::create($requestData);

        return redirect('plansetdetailusers')->with('flash_message', 'PlanSetDetailUser added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $plansetdetailuser = PlanSetDetailUser::findOrFail($id);

        return view('plansetdetailusers.show', compact('plansetdetailuser'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $plansetdetailuser = PlanSetDetailUser::findOrFail($id);
        $plansetdetailRws = PlanSetDetail::where('status', 'Active')->get();
        $plansetdetails = array();
        foreach ($plansetdetailRws as $plansetdetailObj) {
            $plansetdetails[$plansetdetailObj->id] = $plansetdetailObj->planset->name . ' - ' . $plansetdetailObj->dep->name;
        }

        return view('plansetdetailusers.edit', compact('plansetdetailuser', 'plansetdetails', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $plansetdetailuser = PlanSetDetailUser::findOrFail($id);
        $plansetdetailuser->update($requestData);

        return redirect('plansetdetailusers')->with('flash_message', 'PlanSetDetailUser updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        PlanSetDetailUser::destroy($id);

        return redirect('plansetdetailusers')->with('flash_message', 'PlanSetDetailUser deleted!');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function adduser($plansetdetailid)
    {
        $plansetdetailRws = PlanSetDetail::where('status', 'Active')->get();
        $plansetdetails = array();
        foreach ($plansetdetailRws as $plansetdetailObj) {
            $plansetdetails[$plansetdetailObj->id] = $plansetdetailObj->planset->name . ' - ' . $plansetdetailObj->dep->name;
        }

        $plansetdetail = PlanSetDetail::findOrFail($plansetdetailid);

        $users = User::pluck('name', 'id');
        return view('plansetdetailusers.adduser', compact('plansetdetails', 'users', 'plansetdetail'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function adduserAction(Request $request,$plansetdetailid)
    {

        $requestData = $request->all();

        $requestData['plan_detail_set_id'] = $plansetdetailid;

        $plansetdetail = PlanSetDetail::findOrFail($plansetdetailid);

        PlanSetDetailUser::create($requestData);

        return redirect('plansets/'. $plansetdetail->plan_set_id)->with('flash_message', 'PlanSetDetailUser added!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteuser($id)
    {

        $plansetdetailuser = PlanSetDetailUser::findOrFail($id);

        $plansetid = $plansetdetailuser->plansetdetail->plan_set_id;

        PlanSetDetailUser::destroy($id);

        return redirect('plansets/'. $plansetid)->with('flash_message', 'PlanSetDetailUser deleted!');
    }

}
