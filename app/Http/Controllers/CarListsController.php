<?php

namespace App\Http\Controllers;

use App\Models\CarSet;
use App\Models\PlanSetDetail;
use App\Models\AuditChk;
use App\Models\CarAudit;
use App\Models\BrcGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Mail\Send2Auditor;
use Illuminate\Support\Facades\Mail;

class CarListsController extends Controller
{
    public function list(Request $request){
        $planset = $request->get('planset');
        $perPage = 25;
        if(!empty($planset)){
            $carsets = CarSet::where('plan_detail_set_id', $planset)->latest()->paginate($perPage);
        }else{
            $carsets = CarSet::latest()->paginate($perPage);
        }
        

        return view('carlists.index', compact('carsets'));
    }

    public function detail($id)
    {
        $carset = CarSet::findOrFail($id);

        $plansetdetail_id = $carset->plan_detail_set_id;

        $plansetdetail = PlanSetDetail::findOrFail($plansetdetail_id);

        $auditListRaw = AuditChk::where('plan_detail_set_id', $plansetdetail_id)->get();

        $auditByGroup = array();
        $auditByall = array();
        foreach ($auditListRaw as $auditListObj) {
            if(trim($auditListObj->ans) != 'Y' && !empty($auditListObj->ans)){
                
                $auditByGroup[$auditListObj->brcgroup->name]['plan_detail_set_id'] = $auditListObj->plan_detail_set_id;
                $auditByGroup[$auditListObj->brcgroup->name]['brc_group_id'] = $auditListObj->brc_group_id;
                $auditByGroup[$auditListObj->brcgroup->name]['all'][] = $auditListObj;
                if ($auditListObj->status == 'done') {
                    $auditByGroup[$auditListObj->brcgroup->name]['done'][] = $auditListObj;

                    $auditByall['done'][] = $auditListObj;
                }
            }
        }
        $carByAll = array();
        $carByGroup = array();
        foreach($carset->caraudits as $carauditObj) {
            if(isset($carauditObj->brcgroup->name)){
                $carByGroup[$carauditObj->brcgroup->name]['done'][] = $carauditObj;
            }
            $carByAll['done'][] = $carauditObj;
        }

        return view('carlists.detail', compact('auditByGroup', 'plansetdetail', 'auditByall', 'carset', 'carByGroup', 'carByAll'));

    }

    public function reply($id){

        $carset = CarSet::findOrFail($id);

      //  $user = Auth::user();

        $checklistans = array(
            'Y' => 'มีการปฏิบัติสอดคล้องกับข้อกำหนด',
            'N' => 'ไม่มีการนำไปปฏิบัติตามข้อกำหนด',
            'Ob' => 'ข้อเสนอแนะเพื่อปรับปรุง'
        );

        $checklistcar = array(
            'critical' => 'Critical CAR',
            'major' => 'Major CAR',
            'minor' => 'Minor CAR',
            'observation' => 'Observation',
            'off' => 'Off record'
        );

        $plansetdetail = PlanSetDetail::findOrFail($carset->plan_detail_set_id);

        $auditChks = AuditChk::where('plan_detail_set_id', $carset->plan_detail_set_id)->where('brc_dep_id', $carset->dep_id)->get();

        //dd($auditChks->count());

        return view('carlists.reply', compact('plansetdetail', 'auditChks', 'checklistans', 'checklistcar', 'carset'));
    }

    public function replyAction(Request $request,$id){
        $carset = CarSet::findOrFail($id);

        $planSetDetailId = $carset->plan_detail_set_id;
        $brc_group_id = $carset->dep_id;

        $requestData = $request->all();
        $user = Auth::user();
        $auditChks = AuditChk::where('plan_detail_set_id', $planSetDetailId)->where('brc_group_id', $brc_group_id)->get();
        foreach ($auditChks as $auditChkObj) {
            $tmpCar = array();

            $tmpCar['car_set_id'] = $id;
            $tmpCar['audit_chk_id'] = $auditChkObj->id;

            if (isset($requestData['car_date_' . $auditChkObj->id]) && !empty($requestData['car_date_' . $auditChkObj->id])) {
                $tmpCar['car_date'] = $requestData['car_date_' . $auditChkObj->id];
                $tmpCar['status'] = 'Process';
                $tmpCar['ans_user_id'] = $user->id;
            }

            if (isset($requestData['car_prevention_' . $auditChkObj->id]) && !empty($requestData['car_prevention_' . $auditChkObj->id])) {
                $tmpCar['prevention'] = $requestData['car_prevention_' . $auditChkObj->id];
                $tmpCar['status'] = 'Process';
                $tmpCar['ans_user_id'] = $user->id;
            }

            if (isset($requestData['car_correction_' . $auditChkObj->id]) && !empty($requestData['car_correction_' . $auditChkObj->id])) {
                $tmpCar['correction'] = $requestData['car_correction_' . $auditChkObj->id];
                $tmpCar['status'] = 'Process';
                $tmpCar['ans_user_id'] = $user->id;
            }

            if (isset($requestData['car_corrective_action_' . $auditChkObj->id]) && !empty($requestData['car_corrective_action_' . $auditChkObj->id])) {
                $tmpCar['corrective_action'] = $requestData['car_corrective_action_' . $auditChkObj->id];
                $tmpCar['status'] = 'Process';
                $tmpCar['ans_user_id'] = $user->id;
            }

            for ($i=1; $i <= 3; $i++) { 
                if (isset($requestData['remove_path' . $i . '_' . $auditChkObj->id]) && !empty($requestData['remove_path' . $i . '_' . $auditChkObj->id])) {
                    
                }
            
                if ($request->hasFile('car_pic'.$i.'_' . $auditChkObj->id)) {
                    $image = $request->file('car_pic' . $i . '_' . $auditChkObj->id);
                    $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
                    $destinationPath = public_path('images/cars/' . $id);
                    $image->move($destinationPath, $name);

                    $tmpCar['pic' . $i . ''] = $name;
                    $tmpCar['pic' . $i . '_path'] = 'images/cars/' . $id  . "/" . $name;
                    $tmpCar['status'] = 'Process';
                    $tmpCar['ans_user_id'] = $user->id;
                }
            }

            $chk = CarAudit::where('car_set_id',$id)->where('audit_chk_id', $auditChkObj->id)->first();
            if(!empty($chk)){
                $chk->update($tmpCar);
            }else{
                CarAudit::create($tmpCar);
            }
            
        }
    }

    public function replyGroup($id,$group_id)
    {

        $brcgroup = BrcGroup::findOrFail($group_id);

        $carset = CarSet::findOrFail($id);

        $planSetDetailId = $carset->plan_detail_set_id;
        $brc_group_id = $carset->dep_id;

        $checklistans = array(
            'Y' => 'มีการปฏิบัติสอดคล้องกับข้อกำหนด',
            'N' => 'ไม่มีการนำไปปฏิบัติตามข้อกำหนด',
            'Ob' => 'ข้อเสนอแนะเพื่อปรับปรุง'
        );

        $checklistcar = array(
            'critical' => 'Critical CAR',
            'major' => 'Major CAR',
            'minor' => 'Minor CAR',
            'observation' => 'Observation',
            'off' => 'Off record'
        );

        $plansetdetail = PlanSetDetail::findOrFail($carset->plan_detail_set_id);

        $auditChks = AuditChk::where('plan_detail_set_id', $planSetDetailId)
            ->where('brc_dep_id', $brc_group_id)
            ->where('brc_group_id', $group_id)
            ->where('ans', '!=', 'Y')
            ->where('ans', '<>', '')
            ->get();

        $carlistdata = array();
        $carlistdatarw = CarAudit::where('car_set_id', $id)->where('brc_group_id', $group_id)->get();
        foreach($carlistdatarw as $carlistobj){
            $carlistdata[$carlistobj->audit_chk_id] = $carlistobj;
        }
        //dd($auditChks->count());

        return view('carlists.replygroup', compact('plansetdetail', 'auditChks', 'checklistans', 'checklistcar', 'carset', 'brcgroup', 'carlistdata'));
    }

    public function replyGroupAction(Request $request, $id, $group_id)
    {
        $carset = CarSet::findOrFail($id);

        $planSetDetailId = $carset->plan_detail_set_id;
        $brc_group_id = $carset->dep_id;

        $requestData = $request->all();

        // /dd($requestData);

        $user = Auth::user();
        $auditChks = AuditChk::where('plan_detail_set_id', $planSetDetailId)
            ->where('brc_dep_id', $brc_group_id)
            ->where('brc_group_id', $group_id)
            ->where('ans', '!=', 'Y')
            ->where('ans', '<>', '')
            ->get();
        foreach ($auditChks as $auditChkObj) {
            $tmpCar = array();

            $tmpCar['car_set_id'] = $id;
            $tmpCar['brc_group_id'] = $group_id;
            $tmpCar['audit_chk_id'] = $auditChkObj->id;

            if (isset($requestData['car_date_' . $auditChkObj->id]) && !empty($requestData['car_date_' . $auditChkObj->id])) {
                $tmpCar['car_date'] = $requestData['car_date_' . $auditChkObj->id];
                $tmpCar['status'] = 'Process';
                $tmpCar['ans_user_id'] = $user->id;
            }

            if (isset($requestData['car_prevention_' . $auditChkObj->id]) && !empty($requestData['car_prevention_' . $auditChkObj->id])) {
                $tmpCar['prevention'] = $requestData['car_prevention_' . $auditChkObj->id];
                $tmpCar['status'] = 'Process';
                $tmpCar['ans_user_id'] = $user->id;
            }

            if (isset($requestData['car_correction_' . $auditChkObj->id]) && !empty($requestData['car_correction_' . $auditChkObj->id])) {
                $tmpCar['correction'] = $requestData['car_correction_' . $auditChkObj->id];
                $tmpCar['status'] = 'Process';
                $tmpCar['ans_user_id'] = $user->id;
            }

            if (isset($requestData['car_corrective_action_' . $auditChkObj->id]) && !empty($requestData['car_corrective_action_' . $auditChkObj->id])) {
                $tmpCar['corrective_action'] = $requestData['car_corrective_action_' . $auditChkObj->id];
                $tmpCar['status'] = 'Process';
                $tmpCar['ans_user_id'] = $user->id;
            }

            for ($i = 1; $i <= 3; $i++) {
                if (isset($requestData['remove_path' . $i . '_' . $auditChkObj->id]) && !empty($requestData['remove_path' . $i . '_' . $auditChkObj->id])) {
                }

                if ($request->hasFile('car_pic' . $i . '_' . $auditChkObj->id)) {
                    $image = $request->file('car_pic' . $i . '_' . $auditChkObj->id);
                    $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
                    $destinationPath = public_path('images/cars/' . $id);
                    $image->move($destinationPath, $name);

                    $tmpCar['pic' . $i . ''] = $name;
                    $tmpCar['pic' . $i . '_path'] = 'images/cars/' . $id  . "/" . $name;
                    $tmpCar['status'] = 'Process';
                    $tmpCar['ans_user_id'] = $user->id;
                }
            }

            $chk = CarAudit::where('car_set_id', $id)->where('audit_chk_id', $auditChkObj->id)->first();
            if (!empty($chk)) {
                if($chk->status != "Close"){
                    $chk->update($tmpCar);
                }                
            } else {
                CarAudit::create($tmpCar);
            }
        }

        return redirect('cars/detail/'. $id);
    }

    public function checklistshows($id)
    {

        
      $carset = CarSet::findOrFail($id);
    //   /  dd($carset);
        //  $user = Auth::user();

        $checklistans = array(
            'Y' => 'มีการปฏิบัติสอดคล้องกับข้อกำหนด',
            'N' => 'ไม่มีการนำไปปฏิบัติตามข้อกำหนด',
            'Ob' => 'ข้อเสนอแนะเพื่อปรับปรุง'
        );

        $checklistcar = array(
            'critical' => 'Critical CAR',
            'major' => 'Major CAR',
            'minor' => 'Minor CAR',
            'observation' => 'Observation',
            'off' => 'Off record'
        );

        $plansetdetail = PlanSetDetail::findOrFail($carset->plan_detail_set_id);

        $auditChks = AuditChk::where('plan_detail_set_id', $carset->plan_detail_set_id)
        ->where('brc_dep_id', $carset->dep_id)
            ->where('ans', '!=', 'Y')
            ->where('ans', '<>', '')
        ->get();

        $carlistdata = array();
        $carlistdatarw = CarAudit::where('car_set_id', $id)->get();
        foreach ($carlistdatarw as $carlistobj) {
            $carlistdata[$carlistobj->audit_chk_id] = $carlistobj;
        }
        //dd($auditChks->count());

        return view('carlists.checklistshows', compact('plansetdetail', 'auditChks', 'checklistans', 'checklistcar', 'carset', 'carlistdata'));
    }

    public function sendforclosecar($id){
        $carset = CarSet::findOrFail($id);

        return view('carlists.sendforclosecar', compact('carset'));
    }

    public function sendforclosecarAction(Request $request,$id)
    {
        $requestData = $request->all();

        $carset = CarSet::findOrFail($id);

        $carset->returncar_date = \Carbon\Carbon::parse($requestData['returncar_date'])->format('Y-m-d H:i');
        $carset->returncar_note = $requestData['returncar_note'];
        $carset->status = 'Review';
        $carset->update();

        foreach ($carset->caraudits as $carauditObj) {
            $carauditObj->status = 'Review';
            $carauditObj->update();
        }

        

        //Send Email to auditee

        //$ftStaff = config('myconfig.emaillist');

        $mailObj = array();

        $mailObj['carset'] = $carset;
        $mailObj['subject'] = "[QA Internal Audit] ตอบ Car แผนก " . $carset->plansetdetail->dep->name;

        Mail::to([
            'PKP' => 'parinya.k@lannaagro.com',
        ])->send(new Send2Auditor($mailObj));

        return redirect('/cars/detail/' . $id);

    }

    public function mycarslist($plandetailid,$dep_id){
        $perPage = 25;
            $carsets = CarSet::where('plan_detail_set_id', $plandetailid)
            ->where('dep_id', $dep_id)
            ->latest()->paginate($perPage);
        


        return view('carlists.myindex', compact('carsets'));
    }


    public function mycarslistforclose($plandetailid, $dep_id)
    {
        $perPage = 25;
        $carsets = CarSet::where('plan_detail_set_id', $plandetailid)
            ->where('dep_id', $dep_id)
            ->latest()->paginate($perPage);



        return view('carlists.myindexclose', compact('carsets'));
    }

    public function checklistshowsforclose($id)
    {


        $carset = CarSet::findOrFail($id);
        //   /  dd($carset);
        //  $user = Auth::user();

        $checklistans = array(
            'Y' => 'มีการปฏิบัติสอดคล้องกับข้อกำหนด',
            'N' => 'ไม่มีการนำไปปฏิบัติตามข้อกำหนด',
            'Ob' => 'ข้อเสนอแนะเพื่อปรับปรุง'
        );

        $checklistcar = array(
            'critical' => 'Critical CAR',
            'major' => 'Major CAR',
            'minor' => 'Minor CAR',
            'observation' => 'Observation',
            'off' => 'Off record'
        );

        $plansetdetail = PlanSetDetail::findOrFail($carset->plan_detail_set_id);

        $auditChks = AuditChk::where('plan_detail_set_id', $carset->plan_detail_set_id)
            ->where('brc_dep_id', $carset->dep_id)
            ->where('ans', '!=', 'Y')
            ->where('ans', '<>', '')
            ->where('status', '!=', 'Close')
            ->get();

        $carlistdata = array();
        $carlistdatarw = CarAudit::where('car_set_id', $id)->get();
        foreach ($carlistdatarw as $carlistobj) {
            $carlistdata[$carlistobj->audit_chk_id] = $carlistobj;
        }
        //dd($auditChks->count());

        return view('carlists.checklistshowsforclose', compact('plansetdetail', 'auditChks', 'checklistans', 'checklistcar', 'carset', 'carlistdata'));
    }

    public function changestatus($catsetid,$status){
        $carset = CarSet::findOrFail($catsetid);
        if ($status == 'approve') {
            $carset->status = 'Approve';
            $carset->update();

            $plansetdetail = PlanSetDetail::findOrFail($carset->plan_detail_set_id);
            $plansetdetail->status = 'QMR';
            $plansetdetail->update();

        } elseif ($status == 'reject') {
            $carset->status = 'Reject';
            $carset->update();

            $plansetdetail = PlanSetDetail::findOrFail($carset->plan_detail_set_id);
            $plansetdetail->status = 'RECHECK';
            $plansetdetail->update();
        } 
        return redirect('/plansets/' . $plansetdetail->plan_set_id);
    }
         
    public function closecarAction(Request $request,$carsetid){
        $requestData = $request->all();

        $carset = CarSet::findOrFail($carsetid);

        foreach($carset->caraudits as $auditChkObj) {
            if(isset($requestData['close-'. $auditChkObj->audit_chk_id]) || $auditChkObj->status == "Close"){
                $auditChkObj->status = "Close";

                $auditChkObj->auditchk->status = "Close";
                $auditChkObj->auditchk->update();
            }else{
                $auditChkObj->status = "Recheck";

                $auditChkObj->auditchk->status = "Recheck";
                $auditChkObj->auditchk->update();

                $carset->status = "Recheck";
                $carset->update();
            }
            if (isset($requestData['feedbackcar-' . $auditChkObj->audit_chk_id])) {
                $auditChkObj->feedbackcar = $requestData['feedbackcar-' . $auditChkObj->audit_chk_id];
            }else{
                $auditChkObj->feedbackcar = "";
            }
            //dd($auditChkObj);
            $auditChkObj->update();
        }

        return redirect('cars/mycarslist/'. $carset->plan_detail_set_id.'/' . $carset->dep_id);
    }

    public function closethiscarset($carsetid){
        $carset = CarSet::findOrFail($carsetid);
        
        $carset->plansetdetail->status = 'Close';
        $carset->plansetdetail->update();

        $carset->status = 'Close';
        $carset->update();

        return redirect('audits/listbygroup/' . $carset->plan_detail_set_id );
    }
}
