<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\BrcGroup;
use Illuminate\Http\Request;

class BrcGroupsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $brcgroups = BrcGroup::latest()->paginate($perPage);
        } else {
            $brcgroups = BrcGroup::latest()->paginate($perPage);
        }

        return view('brcgroups.index', compact('brcgroups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('brcgroups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        BrcGroup::create($requestData);

        return redirect('brcgroups')->with('flash_message', 'BrcGroup added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $brcgroup = BrcGroup::findOrFail($id);

        return view('brcgroups.show', compact('brcgroup'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $brcgroup = BrcGroup::findOrFail($id);

        return view('brcgroups.edit', compact('brcgroup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $brcgroup = BrcGroup::findOrFail($id);
        $brcgroup->update($requestData);

        return redirect('brcgroups')->with('flash_message', 'BrcGroup updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        BrcGroup::destroy($id);

        return redirect('brcgroups')->with('flash_message', 'BrcGroup deleted!');
    }
}
