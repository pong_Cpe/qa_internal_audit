<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\PlanSetDetail;
use App\Models\User;
use App\Models\PlanSet;
use App\Models\Dep;
use App\Models\BrcSet;
use Illuminate\Http\Request;

class PlanSetDetailsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $plansetdetails = PlanSetDetail::latest()->paginate($perPage);
        } else {
            $plansetdetails = PlanSetDetail::latest()->paginate($perPage);
        }

        return view('plansetdetails.index', compact('plansetdetails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    { 

        $leadauditors = User::whereIn('group_id',array(3,4))->pluck('name', 'id');
        $auditees = User::pluck('name', 'id');
        $plansets = PlanSet::where('status', 'Active')->pluck('name', 'id');
        $deps = Dep::where('status', 'Active')->pluck('name', 'id');
        return view('plansetdetails.create',compact('leadauditors', 'plansets','deps', 'auditees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        //compare
        $ps = \Carbon\Carbon::parse($requestData['plan_start']);
        $pe = \Carbon\Carbon::parse($requestData['plan_end']);
        $as = \Carbon\Carbon::parse($requestData['act_start']);
        $ae = \Carbon\Carbon::parse($requestData['act_end']);
     
        if($pe->greaterThanOrEqualTo($ps) && $ae->greaterThanOrEqualTo($as)){

            $requestData['plan_start'] = \Carbon\Carbon::parse($requestData['plan_start'])->format('Y-m-d H:i');
            $requestData['plan_end'] = \Carbon\Carbon::parse($requestData['plan_end'])->format('Y-m-d H:i');
            $requestData['act_start'] = \Carbon\Carbon::parse($requestData['act_start'])->format('Y-m-d H:i');
            $requestData['act_end'] = \Carbon\Carbon::parse($requestData['act_end'])->format('Y-m-d H:i');
            
            PlanSetDetail::create($requestData);

        }

        return redirect('plansetdetails')->with('flash_message', 'PlanSetDetail added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $plansetdetail = PlanSetDetail::findOrFail($id);

        return view('plansetdetails.show', compact('plansetdetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit(Request $request,$id)
    {
        $plansetdetail = PlanSetDetail::findOrFail($id);
        $leadauditors = User::whereIn('group_id', array(3, 4))->pluck('name', 'id');
        $auditees = User::pluck('name', 'id');
        $plansets = PlanSet::where('status', 'Active')->pluck('name', 'id');
        $deps = Dep::where('status', 'Active')->pluck('name', 'id');
        $brcsets = BrcSet::where('status', 'Active')->pluck('name', 'id');

        $prevurl = $request->input('prevurl');        

        return view('plansetdetails.edit', compact('plansetdetail', 'leadauditors', 'plansets', 'deps', 'auditees', 'prevurl', 'brcsets'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();

        $ps = \Carbon\Carbon::parse($requestData['plan_start']);
        $pe = \Carbon\Carbon::parse($requestData['plan_end']);
        $as = \Carbon\Carbon::parse($requestData['act_start']);
        $ae = \Carbon\Carbon::parse($requestData['act_end']);

        if ($pe->greaterThanOrEqualTo($ps) && $ae->greaterThanOrEqualTo($as)) {

            $requestData['plan_start'] = \Carbon\Carbon::parse($requestData['plan_start'])->format('Y-m-d H:i');
            $requestData['plan_end'] = \Carbon\Carbon::parse($requestData['plan_end'])->format('Y-m-d H:i');
            $requestData['act_start'] = \Carbon\Carbon::parse($requestData['act_start'])->format('Y-m-d H:i');
            $requestData['act_end'] = \Carbon\Carbon::parse($requestData['act_end'])->format('Y-m-d H:i');

            $plansetdetail = PlanSetDetail::findOrFail($id);
            $plansetdetail->update($requestData);

        }
        
        $prevurl = $request->input('prevurl');   

        if(!empty($prevurl)){
            return redirect($prevurl)->with('flash_message', 'PlanSetDetail updated!');
        }else{
            return redirect('plansetdetails')->with('flash_message', 'PlanSetDetail updated!');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        PlanSetDetail::destroy($id);

        return redirect('plansetdetails')->with('flash_message', 'PlanSetDetail deleted!');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function createwithplan($planid)
    {

        $leadauditors = User::whereIn('group_id', array(3, 4))->pluck('name', 'id');
        $auditees = User::pluck('name', 'id');

        $plansetObj = PlanSet::findOrFail($planid);

        $plansets = PlanSet::where('status', 'Active')->pluck('name', 'id');
        $deps = Dep::where('status', 'Active')->pluck('name', 'id');
        $brcsets = BrcSet::where('status', 'Active')->pluck('name', 'id');
        return view('plansetdetails.createwithplan', compact('leadauditors', 'plansets', 'deps', 'auditees', 'planid', 'brcsets', 'plansetObj'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createwithplanAction(Request $request, $planid)
    {

        $requestData = $request->all();

        $ps = \Carbon\Carbon::parse($requestData['plan_start']);
        $pe = \Carbon\Carbon::parse($requestData['plan_end']);
        $as = \Carbon\Carbon::parse($requestData['act_start']);
        $ae = \Carbon\Carbon::parse($requestData['act_end']);



        if ($pe->greaterThanOrEqualTo($ps) && $ae->greaterThanOrEqualTo($as)) {

            $requestData['plan_start'] = \Carbon\Carbon::parse($requestData['plan_start'])->format('Y-m-d H:i');
            $requestData['plan_end'] = \Carbon\Carbon::parse($requestData['plan_end'])->format('Y-m-d H:i');
            $requestData['act_start'] = \Carbon\Carbon::parse($requestData['act_start'])->format('Y-m-d H:i');
            $requestData['act_end'] = \Carbon\Carbon::parse($requestData['act_end'])->format('Y-m-d H:i');

            PlanSetDetail::create($requestData);
        }

        return redirect('plansets/'.$planid)->with('flash_message', 'PlanSetDetail added!');
    }

    public function deletedetail($id)
    {
        $plansetdetail = PlanSetDetail::findOrFail($id);
        $plansetid = $plansetdetail->plan_set_id;

        PlanSetDetail::destroy($id);

        return redirect('plansets/'. $plansetid)->with('flash_message', 'PlanSetDetail deleted!');
    }
}
