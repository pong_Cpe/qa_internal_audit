<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\PlanSet;
use App\Models\User;
use App\Models\BrcSet;
use Illuminate\Http\Request;

class PlanSetsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $plansets = PlanSet::latest()->paginate($perPage);
        } else {
            $plansets = PlanSet::latest()->paginate($perPage);
        }

        return view('plansets.index', compact('plansets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $users = User::where('group_id',1)->pluck('name', 'id');
        $brcsets = BrcSet::where('status', 'Active')->pluck('name', 'id');
        return view('plansets.create',compact('users', 'brcsets'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $id = PlanSet::create($requestData)->id;

        PlanSet::where('status', 'Active')
        ->update(['status' => 'Inactive']);

        $planset = PlanSet::findOrFail($id);
        $planset->status = 'Active';
        $planset->update();
        
        return redirect('plansets')->with('flash_message', 'PlanSet added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $planset = PlanSet::findOrFail($id);

        return view('plansets.show', compact('planset'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $planset = PlanSet::findOrFail($id);
        $users = User::where('group_id',1)->pluck('name', 'id');
        $brcsets = BrcSet::where('status', 'Active')->pluck('name', 'id');

        return view('plansets.edit', compact('planset', 'users', 'brcsets'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $planset = PlanSet::findOrFail($id);
        $planset->update($requestData);

        return redirect('plansets')->with('flash_message', 'PlanSet updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        PlanSet::destroy($id);

        return redirect('plansets')->with('flash_message', 'PlanSet deleted!');
    }

    public function setactive($id){
        PlanSet::where('status', 'Active')
        ->update(['status'=>'Inactive']);

        $planset = PlanSet::findOrFail($id);
        $planset->status = 'Active';
        $planset->update();

        return redirect('plansets')->with('flash_message', 'PlanSet deleted!');
    }
}
