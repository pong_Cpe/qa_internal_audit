<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarSet extends Model
{
    use HasFactory;
    protected $fillable = ['plan_detail_set_id','dep_id','deathline','round','note','status', 'returncar_date', 'returncar_note'];

    public function plansetdetail()
    {
        return $this->hasOne('App\Models\PlanSetDetail', 'id', 'plan_detail_set_id');
    }

    public function dep()
    {
        return $this->hasOne('App\Models\Dep', 'id', 'dep_id');
    }

    public function caraudits()
    {
        return $this->hasMany('App\Models\CarAudit', 'car_set_id');
    }
}
