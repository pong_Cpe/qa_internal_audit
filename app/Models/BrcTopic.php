<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BrcTopic extends Model
{
    use HasFactory;

    protected $fillable = ['brc_set_id','brc_group_id','seq','ref','topic','type','desc','status'];

    public function brcset()
    {
        return $this->hasOne('App\Models\BrcSet', 'id', 'brc_set_id');
    }

    public function brcgroup()
    {
        return $this->hasOne('App\Models\BrcGroup', 'id', 'brc_group_id');
    }

    public function brcdeps()
    {
        return $this->hasMany('App\Models\BrcDep', 'brc_topic_id');
    }
}
