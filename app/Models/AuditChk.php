<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuditChk extends Model
{
    use HasFactory;
    protected $fillable = ['plan_set_id'
            ,'plan_detail_set_id'
            ,'brc_topic_id'
            ,'brc_dep_id'
            ,'brc_group_id'
            ,'audit_date'
            ,'ans'
            ,'car_type'
            ,'review'
            ,'reviewfile'
            ,'reviewfile_path'
            ,'note'
            ,'status'
            ,'pic'
            ,'pic_path'
            ,'ans_user_id'
            ,'review_user_id'
            ,'approve_user_id'];

    public function brctopic()
    {
        return $this->hasOne('App\Models\BrcTopic', 'id', 'brc_topic_id');
    }

    public function brcgroup()
    {
        return $this->hasOne('App\Models\BrcGroup', 'id', 'brc_group_id');
    }
}
