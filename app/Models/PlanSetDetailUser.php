<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlanSetDetailUser extends Model
{
    use HasFactory;
    protected $fillable = ['plan_detail_set_id','user_id','type'];

    public function plansetdetail()
    {
        return $this->hasOne('App\Models\PlanSetDetail', 'id', 'plan_detail_set_id');
    }

    public function auditor()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}
