<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlanSetDetail extends Model
{
    use HasFactory;

    protected $fillable = ['plan_set_id','dep_id','plan_start','plan_end','act_start','act_end','note','lead_auditor_id', 'auditee_id','status', 'brc_set_id'];

    public function planset()
    {
        return $this->hasOne('App\Models\PlanSet', 'id', 'plan_set_id');
    }

    public function dep()
    {
        return $this->hasOne('App\Models\Dep', 'id', 'dep_id');
    }

    public function leadauditor()
    {
        return $this->hasOne('App\Models\User', 'id', 'lead_auditor_id');
    }

    public function auditee()
    {
        return $this->hasOne('App\Models\User', 'id', 'auditee_id');
    }

    public function auditors()
    {
        return $this->hasMany('App\Models\PlanSetDetailUser', 'plan_detail_set_id');
    }

    public function brcset()
    {
        return $this->hasOne('App\Models\BrcSet', 'id', 'brc_set_id');
    }

    public function audits(){
        return $this->hasMany('App\Models\AuditChk', 'plan_detail_set_id');        
    }
}
