<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BrcDep extends Model
{
    use HasFactory;

    protected $fillable = ['brc_topic_id','dep_id', 'brc_set_id',];

    public function brctopic()
    {
        return $this->hasOne('App\Models\BrcTopic', 'id', 'brc_topic_id');
    }

    public function dep()
    {
        return $this->hasOne('App\Models\Dep', 'id', 'dep_id');
    }

    public function brcset()
    {
        return $this->hasOne('App\Models\BrcSet', 'id', 'brc_set_id');
    }

}
