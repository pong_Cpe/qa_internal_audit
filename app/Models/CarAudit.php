<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarAudit extends Model
{
    use HasFactory;

    protected $fillable = [
        'car_set_id',
        'brc_group_id',
        'audit_chk_id',
        'car_date',
        'prevention',
        'correction',
        'corrective_action',
        'status',
        'pic1',
        'pic1_path',
        'pic2',
        'pic2_path',
        'pic3',
        'pic3_path',
        'ans_user_id',
        'review_user_id',
        'approve_user_id',
        'ref_id', 
        'feedbackcar'
    ];

    public function carset()
    {
        return $this->hasOne('App\Models\CarSet', 'id', 'car_set_id');
    }

    public function brcgroup()
    {
        return $this->hasOne('App\Models\BrcGroup', 'id', 'brc_group_id');
    }

    public function auditchk()
    {
        return $this->hasOne('App\Models\AuditChk', 'id', 'audit_chk_id');
    }
}
