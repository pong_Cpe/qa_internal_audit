<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlanSet extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'desc', 'start_date', 'end_date', 'qmr_id', 'status', 'brc_set_id'
    ];

    public function qmr()
    {
        return $this->hasOne('App\Models\User', 'id', 'qmr_id');
    }

    public function brcset()
    {
        return $this->hasOne('App\Models\BrcSet', 'id', 'brc_set_id');
    }

    public function plansetdetails()
    {
        return $this->hasMany('App\Models\PlanSetDetail', 'plan_set_id');
    }
}
