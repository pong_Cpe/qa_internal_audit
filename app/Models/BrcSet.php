<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BrcSet extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'desc',
        'status'
    ];

    public function brctopics()
    {
        return $this->hasMany('App\Models\BrcTopic', 'brc_set_id');
    }

}
